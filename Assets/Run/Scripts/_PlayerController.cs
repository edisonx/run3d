﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public enum ActionState {
    ACTION_STATE_IDLE = 0,
    ACTION_STATE_PLAY = 1,
    ACTION_STATE_WIN = 2,
    ACTION_STATE_LOSE = 3,
}

public enum EffectState {
    EFFECT_STATE_CLOSE = 0,
    EFFECT_STATE_FIRST = 1,
    EFFECT_STATE_VICTORY = 2,
}

public class _PlayerController : MonoBehaviour {
    public PlayerUI playerUI;
    public GameObject playerUIobj;
    public PlayerFiller playerFiller;
    public GameObject playerProgressObj;

    public Transform effectPos;
    public GameObject firstEffect;
    public GameObject victoryEffect;

    public Transform targetPos;
    public float mSpeed = 1;//速度
    public float BOOST_TIME = 1.0f; // second

    Animator animator;
    NavMeshAgent agent;
    User mUser;

    bool isView = true;

    public bool isDeath = false;
    public bool isPlayAnim = false;

    public ActionState mActionState;
    public EffectState mEffectState;

    public int[] speedGears = new[] { 10, 20, 40, 80, 160, 320, 640, 1280, 2560, 5120 };
    private int curFrequency_Index = 0;
    public float[] fortifySpeedGears = new[] { 0.5f, 1.0f, 2.0f, 4.0f, 6.5f, 7.0f, 8.0f, 8.5f, 9.0f, 9.5f };
    public float fortifySpeed = 0;
    private float timer = 0;

    private float speedTimer = 0;
    float mIntegral = 1;//积分
    float mIntegralMin = 0;
    public float mIntegralRatio = 0.1f;//积分比例
    public float mAttenuationRatio = 0.4f;//衰减率
    public float mAddIntegral = 0.5f;

    public enum FrequencyState {
        FRQQUENCY_NORMAL = 0,
        FRQQUENCY_UNNORMAL = 1,
    }

    public FrequencyState playerFS;

    void Awake () {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();

        mActionState = ActionState.ACTION_STATE_IDLE;
        playerFS = FrequencyState.FRQQUENCY_NORMAL;

        //playerUI = Resources.Load<PlayerUI>("Prefab/PlayerUI");
        Object playerUIprefab = Resources.Load("Prefabs/UI/PlayerUI", typeof(GameObject));
        playerUIobj = Instantiate(playerUIprefab) as GameObject;
        if (playerUIobj != null && Global.GameManager != null) {
            playerUIobj.transform.SetParent(Global.GameManager.UiMgr/*UIManager.Instance*/.ThumbParent, false);
            playerUI = playerUIobj.GetComponent<PlayerUI>();
            playerUI.target = gameObject;
            Global.GameManager.UiMgr/*UIManager.Instance*/.playerUIs.Add(playerUI);
        }

        Object obj = Resources.Load("Prefabs/UI/PlayerFiller", typeof(GameObject));
        playerProgressObj = Instantiate(obj) as GameObject;
        if (playerProgressObj != null && Global.GameManager != null) {
            playerProgressObj.transform.SetParent(Global.GameManager.UiMgr/*UIManager.Instance*/.ProgressParent, false);
            playerFiller = playerProgressObj.GetComponent<PlayerFiller>();
            Global.GameManager.UiMgr/*UIManager.Instance*/.playerProgresses.Add(playerFiller);
        }
    }

    // Use this for initialization
    void Start () {
        targetPos = Global.GameManager.targetObj.transform;

        effectPos = this.transform.Find("EffectPos");
        firstEffect = effectPos.Find("fx_Summoner_o").gameObject;
        victoryEffect = effectPos.Find("fx_Summoner_m").gameObject;

        agent.SetDestination(new Vector3(targetPos.position.x + 1.5f, transform.position.y, transform.position.z));
        agent.isStopped = true;
        agent.speed = mSpeed;
    }

    // Update is called once per frame
    void Update () {
        if (IsInView(transform.position)) {
            //DebugConsole.Log("目前本物体在摄像机范围内");
            if (!isView) {

            } else {

            }
        } else {
            //DebugConsole.Log("目前本物体不在摄像机范围内");
            //agent.velocity = new Vector3(1 + Global.GameManager.PLAYER_SPEED_MAX, 0, 0);
            //agent.speed = Global.GameManager.PLAYER_SPEED_MAX + 1;
            //transform.GetComponent<Rigidbody>().AddForce(Vector3.right * Global.GameManager.PLAYER_SPEED_MAX, ForceMode.Force);
            if (Global.GameManager.FirstPlayer)
                gameObject.transform.localPosition = Vector3.Lerp(transform.position,
                    new Vector3(Global.GameManager.FirstPlayer.transform.position.x, transform.position.y, transform.position.z),
                    1.0f * Time.deltaTime);
        }

        timer += Time.deltaTime;
        //if (timer >= 0.75f) {
        //    //if (curFrequency_Index > 0) {
        //    //    curFrequency_Index--;
        //    //}
        //} else
        //if (timer > 0.5f) {
        //    //if (mUser != null && mUser.SocketID < 0 && Global.GameManager.isGaming()) {
        //    //    this.attachPilotActions();
        //    //}
        //    float attenuation = mSpeed * mAttenuationRatio;
        //    mIntegral -= attenuation;

        //    if (mUser != null && mUser.SocketID > 0 && Global.GameManager.isGaming())
        //        DebugConsole.LogError(mUser.NickName + " * " + mIntegral + " * speed:" + mSpeed);

        //    timer = 0;
        //} else 
        if (timer > 0.1f) {
            /*
            if (mIntegral > mIntegralMin) {
                float attenuation = mSpeed * mAttenuationRatio;
                mIntegral -= attenuation;
            } else {
                mIntegral = mIntegralMin;
            }
            */

            if (mUser != null && mUser.SocketID < 0 && Global.GameManager.isGaming()) {
                this.attachPilotActions();
            }
            timer = 0;
        }

        speedTimer += Time.deltaTime;
        //if (speedTimer > 0.005f) 
        {
            float attenuation = mSpeed * mAttenuationRatio;
            mIntegral -= attenuation;

            if (mUser != null && mUser.SocketID > 0 && Global.GameManager.isGaming())

                speedTimer = 0;
        }

        //if (mIntegral > mIntegral / (mIntegral * mIntegralRatio)) {
        //    mSpeed = mIntegral * mIntegralRatio;
        //}
        //else {
        //    mIntegral = mIntegral / (mIntegral * mIntegralRatio);
        //}

        mSpeed = mIntegral * mIntegralRatio;
        agent.speed = mSpeed;

        //DebugConsole.LogError("Speed:" + mSpeed + " mIntegral:" + mIntegral);

        if (isDeath) {
            Global.GameManager.mPlayingList.Remove(this);
            destroyPlayer();
        }
    }

    private void FixedUpdate () {
        if (targetPos != null) {
            if (Global.GameManager.isGaming()) {
                animator.SetBool("isMove", true);
                //agent.enabled = true;
                if (agent.remainingDistance != 0 && agent.remainingDistance <= 1.5f &&
                     mActionState == ActionState.ACTION_STATE_PLAY) {
                    //agent.speed = timer;
                    changeState(ActionState.ACTION_STATE_IDLE);
                } else {
                    //distance = agent.remainingDistance;
                    if (animator.GetCurrentAnimatorStateInfo(0).IsName("Move")) {
                        changeState(ActionState.ACTION_STATE_PLAY);
                        //agent.speed = 1;
                    } else {
                        //agent.speed = 0;
                        changeState(ActionState.ACTION_STATE_IDLE);
                    }
                }
            } else {
                animator.SetBool("isMove", false);
                //agent.enabled = false;
            }
        }
    }

    void OnWillRenderObject () {

    }

    public bool IsInView (Vector3 worldPos) {
        Transform camTransform = Camera.main.transform;
        Vector2 viewPos = Camera.main.WorldToViewportPoint(worldPos);
        Vector3 dir = (worldPos - camTransform.position).normalized;
        float dot = Vector3.Dot(camTransform.forward, dir);         //判断物体是否在相机前面

        if (dot > 0 &&
            viewPos.x >= 0.02 && viewPos.x <= 1 && /*TODO 0.025原本为0 为了把角色限制在屏幕靠里一点 虽然直接这么改太智障了 但是为了赶时间*/
            viewPos.y >= 0 && viewPos.y <= 1)
            return true;
        else
            return false;
    }

    void OnBecameVisible () {
        DebugConsole.Log(this.gameObject.name + "这个物体出现在屏幕里面了");
    }

    void OnBecameInvisible () {
        DebugConsole.Log(this.gameObject.name + "这个物体离开屏幕里面了");
    }

    void attachPilotActions () {
        float value = Global.GameManager.aiControler.getSharkCount() * 4;
        //DebugConsole.Log("attachPilotActions :" + value);
        StartCoroutine(addBoostForce(value));
    }

    public void AddVelocity (float value) {
        agent.velocity = new Vector3(value, 0, 0);
    }

    public float getVelocity () {
        return agent.speed;
    }

    public void SetPlayerDest (float value) {
        agent.SetDestination(new Vector3(targetPos.position.x + value, transform.position.y, transform.position.z));
    }


    public void changeState (ActionState state) {
        //agent.SetDestination(transform.position);
        //if (!Global.GameManager.isGaming())
        //    return;
        switch (state) {
            case ActionState.ACTION_STATE_IDLE:
                agent.isStopped = true;
                break;
            case ActionState.ACTION_STATE_PLAY:
                if (playerFS == FrequencyState.FRQQUENCY_NORMAL) {
                    //agent.enabled = true;
                    agent.isStopped = false;
                    float speed = agent.velocity.magnitude / (mSpeed + fortifySpeedGears[9]);
                    animator.SetFloat("Move", speed);

                    if (playerFiller) {
                        float p = Vector3.Distance(gameObject.transform.position, targetPos.transform.position) /
                                  Vector3.Distance(EnvControler.PLAYER_ORIGIN_POSITION, targetPos.transform.position) * 100f;
                        p = Mathf.Clamp(p, 0, 100);
                        playerFiller.SetProgressbarValue(p);
                        //DebugConsole.Log(" T-T:" + Vector3.Distance(gameObject.transform.position, targetPos.transform.position) +
                        //          " O-T:" + Vector3.Distance(EnvControler.PLAYER_ORIGIN_POSITION, targetPos.transform.position) +
                        //          " -P-:" + p);
                    }
                } else {
                    agent.isStopped = true;
                }
                break;
            case ActionState.ACTION_STATE_WIN:
                agent.SetDestination(transform.position);
                animator.SetBool("Win" + Random.Range(0, 3), true);
                agent.isStopped = true;
                //agent.enabled = false;
                break;
            case ActionState.ACTION_STATE_LOSE:
                agent.SetDestination(transform.position);
                animator.SetBool("Lose" + Random.Range(0, 4), true);
                agent.isStopped = true;
                //agent.enabled = false;
                break;
            default:
                break;
        }
        mActionState = state;
    }

    public IEnumerator addBoostForce (float value) {
        //if (curFrequency_Index >= speedGears[9] && mEffectState != EffectState.EFFECT_STATE_FIRST) {
        //    fortifySpeed = fortifySpeedGears[9];
        //    curFrequency_Index = speedGears[9];
        //}
        //else if (curFrequency_Index >= speedGears[8] && mEffectState != EffectState.EFFECT_STATE_FIRST) {
        //    fortifySpeed = fortifySpeedGears[8];
        //}
        //else if (curFrequency_Index >= speedGears[7]) {
        //    fortifySpeed = fortifySpeedGears[7];
        //}
        //else if (curFrequency_Index >= speedGears[6]) {
        //    fortifySpeed = fortifySpeedGears[6];
        //}
        //else if (curFrequency_Index >= speedGears[5]) {
        //    fortifySpeed = fortifySpeedGears[5];
        //}
        //else if (curFrequency_Index >= speedGears[4]) {
        //    fortifySpeed = fortifySpeedGears[4];
        //}
        //else if (curFrequency_Index >= speedGears[3]) {
        //    fortifySpeed = fortifySpeedGears[3];
        //}
        //else if (curFrequency_Index >= speedGears[2]) {
        //    fortifySpeed = fortifySpeedGears[2];
        //}
        //else if (curFrequency_Index >= speedGears[1]) {
        //    fortifySpeed = fortifySpeedGears[1];
        //}
        //else if (curFrequency_Index >= speedGears[0]) {
        //    fortifySpeed = fortifySpeedGears[0];
        //}
        ////if (curFrequency_Index <= speedGears[speedGears.Length - 1] * 10) {
        ////agent.isStopped = false;
        ////agent.velocity = new Vector3(mSpeed + value, 0, 0);
        //agent.velocity = new Vector3(mSpeed + fortifySpeed, 0, 0);
        //yield return new WaitForSeconds(BOOST_TIME);
        //curFrequency_Index += (int)value;
        ////}
        ////else {
        ////    playerFS = FrequencyState.FRQQUENCY_UNNORMAL;
        ////    agent.velocity = Vector3.zero;
        ////    //nav.isStopped = true;
        ////    animator.SetBool("Rest", true);
        ////    yield return new WaitForSeconds(3.0f);
        ////    animator.SetBool("Rest", false);
        ////    playerFS = FrequencyState.FRQQUENCY_NORMAL;
        ////    curFrequency_Index = 0;
        ////}
        mIntegral += (value * mAddIntegral);
        yield return new WaitForSeconds(1f);
    }

    //void PlayerSpeed() {

    //}

    //void PlayerSpeed() {

    //}

    public float getTargetDest () {
        if (targetPos) {
            //return agent.remainingDistance;
            return Vector3.Distance(transform.position, targetPos.position);
        } else {
            return -1f;
        }
    }

    /// <summary>
    /// 控制特效
    /// </summary>
    /// <param name="state">通过枚举 EffectState 控制特效显示</param>
    public void ShowEffect (EffectState state) {
        mEffectState = state;
        switch (state) {
            case EffectState.EFFECT_STATE_CLOSE:
                firstEffect.gameObject.SetActive(false);
                victoryEffect.gameObject.SetActive(false);
                break;
            case EffectState.EFFECT_STATE_FIRST:
                victoryEffect.gameObject.SetActive(false);
                firstEffect.gameObject.SetActive(true);
                break;
            case EffectState.EFFECT_STATE_VICTORY:
                firstEffect.gameObject.SetActive(false);
                victoryEffect.gameObject.SetActive(true);
                break;
        }
    }

    public bool isPlaying () {
        return mUser.UserState == UserState.Playing;
    }

    public void bindUser (User user) {
        DebugConsole.Log(string.Format("bindUser user(s{0} u{1} {2})", user.SocketID, user.UserID, user.NickName));
        mUser = user;

        string thumbUrl = mUser.ThumbUrl;

        if (mUser != null && string.IsNullOrEmpty(thumbUrl)) {
            //mNickLabel.text = mUser.NickName;
            //mScoreLabel.text = mUser.Score.ToString();
            //if (user.ThumbPng) {
            //    mThumbPng = user.ThumbPng;
            //} else {
            //    Texture2D texture = user.ThumbTex2D;
            //    if (texture) {
            //        Sprite m_sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
            //        mThumbPng.sprite = m_sprite;
            //    } else {

            //    }
            //}
        }
    }

    public User getPlayerUser () {
        return mUser;
    }

    public void unbindUser () {
        DebugConsole.Log(string.Format("unbindUser user(s{0} u{1} {2})", mUser.SocketID, mUser.UserID, mUser.NickName));
        //isDeath = true;
        release();
        mUser = null;
    }

    public void createOne () {
        //TODO 打算创建屏幕显示的头像UI跟随

    }

    public void init () {
        //TODO

    }

    public void idle () {
        //TODO

    }

    public void die () {
        //TODO
        release();
    }

    public void release () {
        //TODO 作为释放
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            DebugConsole.Log("Destroy player on main thread");
            destroyPlayer();
        });
    }

    public void destroyPlayer () {
        //TODO 作为释放
        if (playerUI != null)
            Destroy(playerUI.gameObject);
        if (playerFiller != null)
            Destroy(playerFiller.gameObject);

        Destroy(this.gameObject);
    }

    public void scoring (int index, int score) {
        Global.GameManager.onGameScore(mUser, score);
    }

    public void setUser (User user) {
        mUser = user;
        string thumbUrl = mUser.ThumbUrl;
        if (thumbUrl != null && thumbUrl.Length > 0) {
            //StartCoroutine(user.downloadImage(null, mThumbPng));
            //StartCoroutine(user.downloadImage(thumbUrl, () => {
            UnityMainThreadDispatcher.Instance().Enqueue(user.downloadImage(thumbUrl, () => {
                if (playerUI) {
                    playerUI.setNickName(mUser.NickName);
                    //playerUI.setThumb(thumbUrl);
                    playerUI.setThumb(mUser.ThumbTex2D);
                    playerUI.setScore(mUser.Score.ToString());
                }

                if (playerFiller) {
                    //playerFiller.setThumb(thumbUrl);
                    playerFiller.setThumb(mUser.ThumbTex2D);
                }
            }));
        }
    }

    public User getUser () {
        if (mUser != null) {
            return mUser;
        }
        return null;
    }
}
