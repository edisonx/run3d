﻿using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// 写一个委托方法
/// </summary>
/// <param name="obj"></param>
public delegate void OnTouchEventHandle(GameObject handle, object _obj);

public enum EnumTouchEventType
{
    /// <summary>
    /// 当开始拖拽
    /// </summary>
    OnBeginDrag,
    /// <summary>
    /// 当取消
    /// </summary>
    OnCancel,
    /// <summary>
    /// 当取消选定
    /// </summary>
    OnDeselect,
    /// <summary>
    /// 当拖拽时
    /// </summary>
    OnDrag,
    /// <summary>
    /// 当拖动结束的地方
    /// </summary>
    OnDrop,
    /// <summary>
    /// 当拖动结束时
    /// </summary>
    OnEndDrag,
    /// <summary>
    /// 当移动的时候
    /// </summary>
    OnMove,
    /// <summary>
    /// 当点击时候
    /// </summary>
    OnPointerClick,
    /// <summary>
    /// 当指针摁下
    /// </summary>
    OnPointerDown,
    /// <summary>
    /// 当指针进入
    /// </summary>
    OnPointerEnter,
    /// <summary>
    /// 当指针一移出
    /// </summary>
    OnPointerExit,
    /// <summary>
    /// 当指针抬起
    /// </summary>
    OnPointerUp,
    /// <summary>
    /// 当鼠标滚轮滚动
    /// </summary>
    OnScroll,
    /// <summary>
    /// 当选定时
    /// </summary>
    OnSelect,
    /// <summary>
    /// 当提交按钮被摁下
    /// </summary>
    OnSubmit,
    /// <summary>
    /// 在更新时
    /// </summary>
    OnUpdateSelected
}

/// <summary>
/// 给委托加方法和清除方法的类
/// </summary>
public class TouchHandle
{
    private event OnTouchEventHandle eventHandle = null;
    public void SetHandle(OnTouchEventHandle handle)
    {
        DestoryHandle();
        eventHandle += handle;
    }

    public void DestoryHandle()
    {
        if (eventHandle != null)
        {
            eventHandle -= eventHandle;
            eventHandle = null;
        }
    }

    public void CallEventHandle(GameObject handle, object obj)
    {
        if (eventHandle != null)
        {
            eventHandle(handle, obj);
        }
    }
}


/// <summary>
/// 整合了UGUI信息发送事件
/// </summary>

public class EventTriggerListener : MonoBehaviour,
IPointerEnterHandler,// - OnPointerEnter时调用进入对象的指针
IPointerExitHandler, //- OnPointerExit时调用出口对象的指针
IPointerDownHandler,//- OnPointerDown时调用一个指针按对象
IPointerUpHandler,//- OnPointerUp时调用一个指针释放(称为原按对象)
IPointerClickHandler,// - OnPointerClick称为指针时按下和释放在同一对象
IBeginDragHandler,//- OnBeginDrag呼吁拖动对象, 当拖动即将开始
IDragHandler,// - OnDrag呼吁拖动对象拖动时发生
IEndDragHandler, //- OnEndDrag呼吁拖动对象拖动时完成
IDropHandler,//- OnDrop呼吁对象拖动结束的地方
IScrollHandler, //- OnScroll鼠标滚轮滚动时所需要的
IUpdateSelectedHandler, //- OnUpdateSelected呼吁每个蜱虫所选对象
ISelectHandler,//- OnSelect时调用对象成为选定的对象
IDeselectHandler, //- OnDeselect呼吁所选对象成为竞选资格
IMoveHandler,//- OnMove转会事件发生时调用(左, 右, 上, 下, 等)
ISubmitHandler, //- OnSubmit提交按钮被按下时调用
ICancelHandler//-虚取消按钮被按下时调用

{
    public TouchHandle onBeginDrag,
     onCancel,
     onDeselect,
     onDrag,
     onDrop,
     onEndDrag,
     onMove,
     onPointerClick,
     onPointerDown,
     onPointerEnter,
     onPointerExit,
     onPointerUp,
     onScroll,
     onSelect,
     onSubmit,
     onUpdateSelected;
    /// <summary>
    /// 找到物体，并且给他添加EventTargetListener事件
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    static public EventTriggerListener Get(GameObject obj)
    {
        EventTriggerListener temp;
        if (obj.transform.GetComponent<EventTriggerListener>() == null)
        {
            temp = obj.AddComponent<EventTriggerListener>();
        }
        else
        {
            temp = obj.GetComponent<EventTriggerListener>();
        }

        return temp;
    }

    /// <summary>
    /// - OnBeginDrag呼吁拖动对象, 当拖动即将开始
    /// </summary>
    /// <param name="eventData"></param>
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (onBeginDrag != null)
        {
            onBeginDrag.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// 虚取消按钮被按下时调用
    /// </summary>
    /// <param name="eventData"></param>
    public void OnCancel(BaseEventData eventData)
    {
        if (onCancel != null)
        {
            onCancel.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// OnDeselect呼吁所选对象成为竞选资格
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDeselect(BaseEventData eventData)
    {
        if (onDeselect != null)
        {
            onDeselect.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    ///  - OnDrag呼吁拖动对象拖动时发生
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrag(PointerEventData eventData)
    {
        if (onDrag != null)
        {
            onDrag.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// OnDrop呼吁对象拖动结束的地方
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrop(PointerEventData eventData)
    {
        if (onDrop != null)
        {
            onDrop.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// OnEndDrag呼吁拖动对象拖动时完成
    /// </summary>
    /// <param name="eventData"></param>
    public void OnEndDrag(PointerEventData eventData)
    {
        if (onEndDrag != null)
        {
            onEndDrag.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    ///  OnMove转会事件发生时调用(左, 右, 上, 下, 等)
    /// </summary>
    /// <param name="eventData"></param>
    public void OnMove(AxisEventData eventData)
    {
        if (onMove != null)
        {
            onMove.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// - OnPointerClick称为指针时按下和释放在同一对象
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClick(PointerEventData eventData)
    {
        if (onPointerClick != null)
        {
            onPointerClick.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// - OnPointerDown时调用一个指针按对象
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerDown(PointerEventData eventData)
    {
        if (onPointerDown != null)
        {
            onPointerDown.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// OnPointerEnter 指针进入时
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (onPointerEnter != null)
        {
            onPointerEnter.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// OnPointerExit退出时候
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerExit(PointerEventData eventData)
    {
        if (onPointerExit != null)
        {
            onPointerExit.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// - OnPointerUp时调用一个指针释放(称为原按对象)
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerUp(PointerEventData eventData)
    {
        if (onPointerUp != null)
        {
            onPointerUp.CallEventHandle(this.gameObject, eventData);
        }
    }
    /// <summary>
    /// OnScroll鼠标滚轮滚动时所需要的
    /// </summary>
    /// <param name="eventData"></param>
    public void OnScroll(PointerEventData eventData)
    {
        if (onScroll != null)
        {
            onScroll.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// OnSelect时调用对象成为选定的对象
    /// </summary>
    /// <param name="eventData"></param>
    public void OnSelect(BaseEventData eventData)
    {
        if (onSelect != null)
        {
            onSelect.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// OnSubmit提交按钮被按下时调用
    /// </summary>
    /// <param name="eventData"></param>
    public void OnSubmit(BaseEventData eventData)
    {
        if (onSubmit != null)
        {
            onSubmit.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// 在更新选择
    /// </summary>
    /// <param name="eventData"></param>
    public void OnUpdateSelected(BaseEventData eventData)
    {
        if (onUpdateSelected != null)
        {
            onUpdateSelected.CallEventHandle(this.gameObject, eventData);
        }
    }

    /// <summary>
    /// 设置事件处理（是点击还是拖拽还是其他触发）
    /// </summary>
    /// <param name="type">输入一个触发方法枚举事件</param>
    /// <param name="handle">输入一个要触发的方法</param>
    public void SetEventHandle(EnumTouchEventType type, OnTouchEventHandle handle)
    {
        switch (type)
        {
            case EnumTouchEventType.OnBeginDrag:
                if (onBeginDrag == null)
                {
                    onBeginDrag = new TouchHandle();
                }
                onBeginDrag.SetHandle(handle);
                break;
            case EnumTouchEventType.OnCancel:
                if (onCancel == null)
                {
                    onCancel = new TouchHandle();
                }
                onCancel.SetHandle(handle);

                break;
            case EnumTouchEventType.OnDeselect:
                if (onDeselect == null)
                {
                    onDeselect = new TouchHandle();
                }
                onDeselect.SetHandle(handle);
                break;
            case EnumTouchEventType.OnDrag:
                if (onDrag == null)
                {
                    onDrag = new TouchHandle();
                }
                onDrag.SetHandle(handle);

                break;
            case EnumTouchEventType.OnDrop:
                if (onDrop == null)
                {
                    onDrop = new TouchHandle();
                }
                onDrop.SetHandle(handle);
                break;
            case EnumTouchEventType.OnEndDrag:
                if (onEndDrag == null)
                {
                    onEndDrag = new TouchHandle();
                }
                onEndDrag.SetHandle(handle);
                break;
            case EnumTouchEventType.OnMove:
                if (onMove == null)
                {
                    onMove = new TouchHandle();
                }
                onMove.SetHandle(handle);
                break;
            case EnumTouchEventType.OnPointerClick:
                if (onPointerClick == null)
                {
                    onPointerClick = new TouchHandle();
                }
                onPointerClick.SetHandle(handle);
                break;
            case EnumTouchEventType.OnPointerDown:
                if (onPointerDown == null)
                {
                    onPointerDown = new TouchHandle();
                }
                onPointerDown.SetHandle(handle);
                break;
            case EnumTouchEventType.OnPointerEnter:
                if (onPointerEnter == null)
                {
                    onPointerEnter = new TouchHandle();
                }
                onPointerEnter.SetHandle(handle);
                break;
            case EnumTouchEventType.OnPointerExit:
                if (onPointerExit == null)
                {
                    onPointerExit = new TouchHandle();
                }
                onPointerExit.SetHandle(handle);
                break;
            case EnumTouchEventType.OnPointerUp:
                if (onPointerUp == null)
                {
                    onPointerUp = new TouchHandle();
                }
                onPointerUp.SetHandle(handle);
                break;
            case EnumTouchEventType.OnScroll:
                if (onScroll == null)
                {
                    onScroll = new TouchHandle();
                }
                onScroll.SetHandle(handle);
                break;
            case EnumTouchEventType.OnSelect:
                if (onSelect == null)
                {
                    onSelect = new TouchHandle();
                }
                onSelect.SetHandle(handle);
                break;
            case EnumTouchEventType.OnSubmit:
                if (onSubmit == null)
                {
                    onSubmit = new TouchHandle();
                }
                onSubmit.SetHandle(handle);
                break;
            case EnumTouchEventType.OnUpdateSelected:
                if (onUpdateSelected == null)
                {
                    onUpdateSelected = new TouchHandle();
                }
                onUpdateSelected.SetHandle(handle);
                break;
        }
    }
}
