﻿using System.Collections.Generic;

public class UserClearDelegate {
    public virtual void onUserJoin (User user) { }

    public virtual void onUserClear (int needPilotNum) { }
}

public class UserManager {
    /// <summary>
    /// 允许游戏的最大用户数
    /// </summary>
    public int PLAYER_MAX = 100;
    /// <summary>
    /// 排行榜界面 显示的个数
    /// </summary>
    public int TOP_LIST_SIZE = 10;

    /// <summary>
    /// 等待用户的最大数
    /// </summary>
    public int WAIT_USER_MAX = 0; //100; TODO 需求暂时不需要等待队列
    /// <summary>
    /// 等待界面 显示的个数
    /// </summary>
    public int WAIT_USER_SIZE = 10;

    /// <summary>
    ///TODO: MAX_PILOT_NUM 要在 AIPiloter类中定义
    /// </summary>
    int MAX_PILOT_NUM = 6;

    public int gDefaultNameIndex = 1;

    /// <summary>
    /// 游戏中用户的队列
    /// </summary>
    public List<User> mPlayerQueue;
    /// <summary>
    /// 等待中用户的队列
    /// </summary>
    private List<User> mWaitingQueue;
    /// <summary>
    /// 排行榜用户队列
    /// </summary>
    List<TopRecord> mTopList;

    private static UserManager _instance;

    public delegate void OnPlayerUpdateEvent ();
    public event OnPlayerUpdateEvent OnPlayerUpdateChanged;

    public delegate void OnWaitingUpdateEvent ();
    public event OnWaitingUpdateEvent OnWaitingUpdateChanged;

    UserClearDelegate mClearDelegator;

    public delegate void OnTopUpdateEvent ();
    public event OnTopUpdateEvent OnTopUpdateChanged;

    public static UserManager Instance {
        get {
            if (_instance == null) {
                _instance = new UserManager();
            }
            return _instance;
        }
    }

    private UserManager () {
        mPlayerQueue = new List<User>();
        mWaitingQueue = new List<User>();
        mTopList = new List<TopRecord>();
        mClearDelegator = new UserClearDelegate();
    }

    public void addUser (User user) {
        DebugConsole.Log(string.Format("addUser {0}", user.SocketID));

        if (isWaiting(user.SocketID) || isPlayer(user.SocketID)) {
            //DebugConsole.Log("ignore this user");
            user.release();
            return;
        }

        if (mPlayerQueue.Count < PLAYER_MAX) {
            mPlayerQueue.Add(user);

            Global.GameManager.newPlayer(user.SocketID, user);
            this.sortPlaysWithScore();
            if (mClearDelegator != null) {
                mClearDelegator.onUserJoin(user);
            }
        } else {
            // 满了,进入等待队列
            if (mWaitingQueue.Count < WAIT_USER_MAX) {
                mWaitingQueue.Add(user);
                //TODO 测试委托实现
                //if (mWaitDelegator) {
                //    mWaitDelegator.onWaitingQUpdate();
                //}
                OnWaitingUpdateChanged();

                if (mClearDelegator != null) {
                    mClearDelegator.onUserJoin(user);
                }
            } else {
                // TODO : 全满了, 拒绝用户 暂时注释掉， 为了测试容量
                /* NetworkControler.Instance.kickUser(user.SocketID);
                 user.release();*/
            }
        }
    }

    public bool isPlayer (int Id) {
        for (int i = 0; i < mPlayerQueue.Count; i++) {
            if (mPlayerQueue[i].SocketID == Id) {
                return true;
            }
        }
        return false;
    }

    public void resetTopList () {
        mTopList.Clear();
    }

    public bool isWaiting (int Id) {
        int n = -1;
        return isWaiting(Id, ref n);
    }

    public bool isWaiting (int Id, ref int sequence) {
        sequence = 0;
        for (int i = 0; i < mWaitingQueue.Count; i++) {
            if (mWaitingQueue[i].SocketID == Id) {
                return true;
            }
            sequence++;
        }
        return false;
    }

    public void sortPlaysWithScore () {
        /* This is a c++ function
       sort(mPlayers.begin(), mPlayers.end(), userComp);
        */
        bubbleSortUserComp(mPlayerQueue);
        //TODO 测试委托实现
        //if (mPlayDelegator) {
        //    mPlayDelegator.onPlayerUpdate();
        //}
        if (OnPlayerUpdateChanged != null) OnPlayerUpdateChanged();
    }

    void bubbleSortUserComp (List<User> arry) {
        for (int i = 0; i < arry.Count - 1; i++) {
            for (int j = 0; j < arry.Count - 1 - i; j++) {
                //比较相邻的两个元素，如果前面的比后面的小，则交换位置
                if (arry[j].Score < arry[j + 1].Score) {
                    User temp = arry[j];
                    arry[j] = arry[j + 1];
                    arry[j + 1] = temp;
                }
            }
        }
    }

    public List<User> getUsers () {
        return mPlayerQueue;
    }

    public void removeUserForGameOver (User user) {
        DebugConsole.Log("removeUserForGameOver");

        if (mPlayerQueue.Count > 0) {
            for (int i = 0; i < mPlayerQueue.Count; i++) {
                if (mPlayerQueue[i] == user) {
                    Global.GameManager.releasePlayer(mPlayerQueue[i].SocketID);
                    mPlayerQueue.Remove(mPlayerQueue[i]);
                    NetworkControler.Instance.kickUser(user.SocketID);
                    popPlayerFromWaitingQueue();
                    sortPlaysWithScore();
                    //user.release(); TODO 有类似方法 重复使用
                    return;
                }
            }
        }

        if (mWaitingQueue.Count > 0) {
            for (int i = 0; i < mWaitingQueue.Count; i++) {
                if (mWaitingQueue[i] == user) {
                    mWaitingQueue.Remove(mWaitingQueue[i]);
                    //TODO 测试委托实现
                    //if (mWaitDelegator) {
                    //    mWaitDelegator.onWaitingQUpdate();
                    //}
                    OnWaitingUpdateChanged();
                    user.release();
                    return;
                }
            }
        }

        // disconnect
    }

    public void removeUser4Quit (int Id) {
        DebugConsole.Log("removeUserForQuit");
        if (mPlayerQueue.Count > 0) {
            for (int i = 0; i < mPlayerQueue.Count; i++) {
                if (mPlayerQueue[i].SocketID == Id) {
                    mPlayerQueue.Remove(mPlayerQueue[i]);
                    Global.GameManager.releasePlayer(Id, true);
                    popPlayerFromWaitingQueue();
                    sortPlaysWithScore();
                    return;
                }
            }
        }

        if (mWaitingQueue.Count > 0) {
            for (int i = 0; i < mWaitingQueue.Count; i++) {
                if (mWaitingQueue[i].SocketID == Id) {
                    mWaitingQueue.Remove(mWaitingQueue[i]);
                    //TODO 测试委托实现
                    //if (mWaitDelegator) {
                    //    mWaitDelegator.onWaitingQUpdate();
                    //}
                    OnWaitingUpdateChanged();
                    return;
                }
            }
        }
    }

    public void popPlayerFromWaitingQueue () {
        DebugConsole.Log(string.Format("popPlayerFromWaitingQueue wait={0} play={1}", mWaitingQueue.Count, mPlayerQueue.Count));
        int n = 0;
        if (hasNoRealPlayer(ref n)) {
            if (mClearDelegator != null) {
                mClearDelegator.onUserClear(n);
            }
        }

        if (mPlayerQueue.Count < PLAYER_MAX && mWaitingQueue.Count > 0) {
            User user = mWaitingQueue[0];
            mPlayerQueue.Add(mWaitingQueue[0]);

            mWaitingQueue.RemoveAt(0);

            NetworkControler.Instance.tellUserBeginPlay(user.SocketID);

            Global.GameManager.newPlayer(user.SocketID, user);
            //TODO 测试委托实现
            //if (mWaitDelegator) {
            //    mWaitDelegator.onWaitingQUpdate();
            //}
            OnWaitingUpdateChanged();
        }
    }

    bool hasNoRealPlayer (ref int needPilotNum) {
        if (this.mWaitingQueue.Count == 0 && this.mPlayerQueue.Count < MAX_PILOT_NUM) {
            int pilotCount = 0;
            for (int i = 0; i < mPlayerQueue.Count; i++) {
                if (mPlayerQueue[i].SocketID >= 0) {
                    return false;
                } else {
                    pilotCount++;
                }
            }
            needPilotNum = MAX_PILOT_NUM - pilotCount;
            return true;
        }
        return false;
    }

    public void sortTopList (User user) {
        TopRecord targetTr = null;

        bool shouldInTop = false;
        if (mTopList.Count >= TOP_LIST_SIZE) {
            // full
            if (user.Score > mTopList[mTopList.Count - 1].getScore()) {
                shouldInTop = true;
            }
        } else { // not full
            shouldInTop = true;
        }

        if (!shouldInTop) {
            return;
        }

        if (mTopList.Count > 0) {
            // 判断是否已经在榜
            for (int i = 0; i < mTopList.Count; i++) {
                TopRecord each = mTopList[i];
                if (each.getUserId() == user.UserID) {
                    if (each.getScore() < user.Score) {
                        each.setScore(user.Score);
                        targetTr = each;
                        break;
                    } else {
                        return;
                    }
                }
            }
        }

        // add to top list
        if (targetTr == null) {
            targetTr = new TopRecord(user.UserID, user.NickName, user.ThumbUrl, user.Score);
            if (mTopList.Count >= TOP_LIST_SIZE) {
                mTopList.RemoveAt(mTopList.Count);
            }
            mTopList.Add(targetTr);
        }

        /* This is a c++ function
        sort(mTopList.begin(), mTopList.end(), topRecordComp);
        */
        bubbleSortRecordComp(mTopList);

        OnTopUpdateChanged();
    }

    bool topRecordComp (TopRecord a, TopRecord b) {
        return a.getScore() > b.getScore();
    }

    void bubbleSortRecordComp (List<TopRecord> arry) {
        for (int i = 0; i < arry.Count - 1; i++) {
            for (int j = 0; j < arry.Count - 1 - i; j++) {
                //比较相邻的两个元素，如果前面的比后面的小，则交换位置
                if (arry[j].getScore() < arry[j + 1].getScore()) {
                    TopRecord temp = arry[j];
                    arry[j] = arry[j + 1];
                    arry[j + 1] = temp;
                }
            }
        }
    }

    public List<TopRecord> getTopList () {
        return mTopList;
    }

    public List<User> getWaitingUsers () {
        return mWaitingQueue;
    }

    public bool isTopPlayer (int userId) {
        for (int i = 0; i < mTopList.Count; i++) {
            TopRecord tr = mTopList[i];
            if (tr.getUserId() == userId) {
                return true;
            }
        }
        return false;
    }

    // for game over
    public void removeAllWaitingUsers () {
        DebugConsole.Log("removeAllWaitingUsers");
        if (mWaitingQueue.Count > 0) {

            for (int i = 0; i < mWaitingQueue.Count; i++) {
                User user = mWaitingQueue[i];
                NetworkControler.Instance.kickUser(user.SocketID);
            }

            mWaitingQueue.Clear();

            //TODO 测试委托实现
            //if (mWaitDelegator) {
            //    mWaitDelegator.onWaitingQUpdate();
            //}
            OnWaitingUpdateChanged();
        }
    }
}
