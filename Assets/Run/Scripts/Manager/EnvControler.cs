﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvControler {

    public static Vector3 PLAYER_ORIGIN_POSITION = new Vector3(-110, 0f, 0.05f);

    public static float ZSpace = 1f;
    public static float XBorn = -1.0f;
    public static float ZReviv = 30.0f;

    //	public static float PREFIX_TIME = 1.0f; // second

    //	float mRunTime = 0.0f;

    const float mStepLength = 220f;

    static EnvControler _instance = null;

    List<GameObject> mTracks;
    GameObject mAssistor = null;

    float[] ZWays;
    float[] XWays;

    private EnvControler() {
        mTracks = new List<GameObject>();
        ZWays = new float[UserManager.Instance.PLAYER_MAX];
        XWays = new float[UserManager.Instance.PLAYER_MAX];
        bool negative = false;
        int sign = 0;
        int xSign = 0;
        for (int i = 0; i < UserManager.Instance.PLAYER_MAX; i++) {
            //ZWays[i] = i * EnvControler.ZSpace * (negative ? -1 : 1);
            if (i % 10 == 0) {
                xSign++;
                XWays[i] = EnvControler.XBorn * xSign;
            }
            if (negative) {
                sign++;
                ZWays[i] = EnvControler.ZSpace * sign;
            }
            else {
                ZWays[i] = EnvControler.ZSpace * (-sign);
            }

            negative = !negative;
        }
    }

    public static EnvControler Instance {
        get {
            if (_instance == null) {
                _instance = new EnvControler();
            }
            return _instance;
        }
    }

    public void init() {
        GameObject track = GameObject.Find("Track0");
        if (track) {
            mTracks.Add(track);
        }
        track = GameObject.Find("Track1");
        if (track) {
            mTracks.Add(track);
        }
    }

    //public void createAssistor () {
    //	if (mAssistor == null) {
    //		mAssistor = GameObject.Instantiate (GameManager.Instance.playerPrefab, 
    //			PLAYER_ORIGIN_POSITION + new Vector3 (1.0f, 0f, 0f), 
    //			new Quaternion (0, 0, 0, 0));
    //	}

    //}

    public void updateEnvironment(Vector3 playerPosition) {

        float track0z = mTracks[0].transform.position.z;
        if ((playerPosition.z - track0z) > (mStepLength + 130f)) {
            DebugConsole.Log("updateEnvironment changing");
            GameObject track = mTracks[0];
            mTracks[0].transform.position += new Vector3(0, 0, mStepLength * 2);
            mTracks.RemoveAt(0);
            mTracks.Add(track);
        }
    }

    public Vector3 getOffset(int index) {
        DebugConsole.Log("getOffset index = " + index + " - " + ZWays[index]);
        return new Vector3(XWays[index], 0, ZWays[index]);
    }

    public float baseline() {
        return mAssistor.transform.position.z;
    }

    //	public float calculateSpeed (float speed) {
    //	}
}
