﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopItem : MonoBehaviour {
    TopRecord mTopRecord;
    public Image m_ThumbAvatar;
    public Text m_NickNameLabel;
    public Text m_IndexLabel;
    public Text m_ScoreLabel;
    TopItem mTopItem;

    void Awake() {
        //获取组件
        m_ThumbAvatar = transform.Find("ThumbAvatar").GetComponent<Image>();
        m_NickNameLabel = transform.Find("NickNameLabel").GetComponent<Text>();
        m_IndexLabel = transform.Find("IndexLabel").GetComponent<Text>();
        m_ScoreLabel = transform.Find("ScoreLabel").GetComponent<Text>();
    }

    public TopItem() {

    }

    public TopItem(TopRecord topRecord) {
        setTopItem(topRecord);
    }

    public void setTopItem(TopRecord topRecord) {
        mTopRecord = topRecord;

        string nickname = topRecord.getNickName();

        m_NickNameLabel.text = nickname;

        string thumbUrl = mTopRecord.getThumbUrl();

        //if (Utility.startsWith(thumbUrl, "http")) {
        //    this.GetHttpImg(mTopRecord.getThumbUrl(), 967);
        //} else {
        //    Utility.createThumbWithFile(thumbUrl, mThumbWidth, mThumbWidth, mThumbPosition, this, false);
        //}

        //TODO: 往后需要把头像缓存起来 节省流量
        StartCoroutine(downloadImage(thumbUrl));

        string buff = mTopRecord.getScore().ToString();
        m_ScoreLabel.text = buff;
    }

    IEnumerator downloadImage(string url) {
        WWW www = new WWW(url);
        yield return www;

        Texture2D tex2d = www.texture;
        //将图片保存至缓存路径  
        byte[] pngData = tex2d.EncodeToPNG();                         //将材质压缩成byte流  

        Sprite m_sprite = Sprite.Create(tex2d, new Rect(0, 0, tex2d.width, tex2d.height), new Vector2(0, 0));
        m_ThumbAvatar.sprite = m_sprite;
    }

    public void setIndex(int i) {
        string buff = i.ToString();
        m_IndexLabel.text = buff;

        //int select = i < 4 ? i : 3;
        //for (int i = 0; i < 4; i++) {
        //    Sprite bg = (Sprite)this.getChildByTag(TAG_BACKGROUND + i);
        //    bg.setVisible(select == i ? true : false);
        //}
    }

    public int getUserId() {
        return mTopRecord.getUserId();
    }

    public void setScore(int score) {
        string buff = score.ToString();
        m_ScoreLabel.text = buff;
    }

    //void GetHttpImg(string imgurl, int number) {
    //    HttpRequest request = new HttpRequest();
    //    // required fields
    //    request.setUrl(imgurl.c_str());
    //    request.setRequestType(HttpRequest::Type::GET);
    //    request.setResponseCallback(this, httpresponse_selector(TopItem.onHttpRequestRptImg));
    //    // optional fields
    //    char thisnumber[10] = "";
    //    sprintf(thisnumber, "%d", number);
    //    request.setTag(thisnumber);
    //    HttpClient.Instance().send(request);
    //    request.release();
    //}

    //void onHttpRequestRptImg(HttpClient sender, HttpResponse response) {
    //    char c_tag[20] = "";
    //    sprintf(c_tag, "%s", response.getHttpRequest().getTag());
    //    DebugConsole.Log("%s completed", response.getHttpRequest().getTag());
    //    string str_tag = c_tag;
    //    if (!response) {
    //        return;
    //    }

    //    if (!response.isSucceed()) {
    //        DebugConsole.Log("response failed");
    //        DebugConsole.Log("error buffer: %s", response.getErrorBuffer());
    //        return;
    //    }

    //    List<char> buffer = response.getResponseData();

    //    //create image
    //    Image img = new Image;
    //    img.initWithImageData((unsigned char *) buffer.data(), buffer.size());

    //    //create texture
    //    Texture2D texture = new Texture2D();
    //    bool isImg = texture.initWithImage(img);
    //    img.release();

    //    if (isImg && texture != null) {
    //        Utility.createThumbWithTexture(texture, mThumbWidth, mThumbWidth, mThumbPosition, this, false);
    //        //mUser.releaseCacheTexture();
    //    }
    //}
}