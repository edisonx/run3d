﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {
    public Text mNickLabel;
    public Image mThumbPng;
    public Text mScoreLabel;

    public GameObject target;

    public Vector2 offsetPos = new Vector2(0, 1.75f);
    public RectTransform rectTrans; // 跟随UI
    public Vector2 targetScreenPosition;

    //声明一个Camera 变量
    public Camera mPanelCamera;

    void Awake () {
        //获取血条的 RectTransform 组件
        rectTrans = GetComponent<RectTransform>();
        //获取角色
        //target = GameObject.FindGameObjectWithTag("Player");
        //在此为了省事我就用比较笨的方法获取UICamear了
        mPanelCamera = GameObject.FindGameObjectWithTag("PanelCamera").GetComponent<Camera>();
        mNickLabel = transform.Find("NickLabel").gameObject.GetComponent<Text>();
        mThumbPng = transform.Find("Mask/ThumbPng").gameObject.GetComponent<Image>();
        mScoreLabel = transform.Find("ScoreLabel").gameObject.GetComponent<Text>();
    }

    // Use this for initialization
    void Start () {
        //Canvas canvas = GameObject.FindGameObjectWithTag("MainCanvas").GetComponent<Canvas>();
        //CanvasScaler cs = canvas.GetComponent<CanvasScaler>();
        //DebugConsole.LogError("Screen.width:" + Screen.width + " Screen.height：" + Screen.height);
        //DebugConsole.LogError("CanvasScaler.x:" + cs.referenceResolution.x + " CanvasScaler.y：" + cs.referenceResolution.y);

        //float x = (Screen.width / cs.referenceResolution.x);
        //float y = (Screen.height / cs.referenceResolution.y);
        //DebugConsole.LogError("CanvasScaler.x:" + x + " CanvasScaler.y：" + y);
    }

    // Update is called once per frame
    void Update () {
        if (target || rectTrans) {
            ////获取跟随目标的位置
            //Vector3 tarPos = target.transform.position;
            ////将目标的世界坐标转换为屏幕坐标
            //Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(Camera.main, tarPos);
            ////将获取到的屏幕坐标赋给血条，并加上偏移值
            //rectTrans.position = screenPos + offsetPos;

            //将角色的3D世界坐标转换为 屏幕坐标
            Vector2 screenPos = Camera.main.WorldToScreenPoint(target.transform.position);

            //定义一个接收转换为 UI  2D 世界坐标的变量
            Vector3 followPosition;

            // 使用下面方法转换
            // RectTransformUtility.ScreenPointToWorldPointInRectangle（）
            // 参数1 血条的 RectTransform 组件；
            // 参数2 角色坐标转换的屏幕坐标
            // 参数3 目标摄像机，Canvas的 Render Mode 参数类型设置为 Screen Space - Camera 需要写摄像机参数
            //       将下面方法的第三个参数 写成 Canvas 参数 Render Camera 上挂的摄像机（UICamera）
            // 参数4 接收转换后的坐标，需要提前声明一个 Vector3 参数
            if (RectTransformUtility.ScreenPointToWorldPointInRectangle(rectTrans, screenPos, mPanelCamera, out followPosition)) {



                //将血条的坐标设置为 UI 2D 世界坐标
                targetScreenPosition = followPosition + new Vector3(offsetPos.x, offsetPos.y);
                //限制UI头像水滴在屏幕范围内
                //targetScreenPosition = new Vector2(
                //   Mathf.Clamp(targetScreenPosition.x, -9.5f, 9.5f),
                //   Mathf.Clamp(targetScreenPosition.y, -4.5f, 4.5f));
                rectTrans.position = targetScreenPosition;
            }

            ////超出屏幕范围隐藏头像
            //if (screenPos.x > Screen.width || screenPos.x < 0 ||
            //    screenPos.y > Screen.height || screenPos.y < 0) {
            //    rectTrans.gameObject.SetActive(false);
            //} else {
            //    rectTrans.gameObject.SetActive(true);
            //}


            //Vector2 pos;
            //// 计算中心点坐标（无论UI使用哪种锚点都将其看做是锚点在中心点）
            //// 参数1：canvas
            //// 参数2：想要限制的UI区域的物体位置
            //// 参数3：UI摄像机
            //// 参数4：输出的结果吗
            //// 注意：转换的时候，参数2物体的Z必须为0
            //if (RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, target.transform.position, mPanelCamera, out pos)) {
            //    DebugConsole.LogError("转换pos：" + pos);
            //    //just suitable for the condition witch Anchor at Image center
            //    // 左
            //    float minX = Screen.width / 2 + mThumbPng.rectTransform.localPosition.x - mThumbPng.rectTransform.rect.width / 2.0f;
            //    // 右
            //    float maxX = Screen.width / 2 + mThumbPng.rectTransform.localPosition.x + mThumbPng.rectTransform.rect.width / 2.0f;
            //    // 上
            //    float minY = Screen.height / 2 - mThumbPng.rectTransform.localPosition.y - mThumbPng.rectTransform.rect.height / 2.0f;
            //    // 下
            //    float maxY = Screen.height / 2 - mThumbPng.rectTransform.localPosition.y + mThumbPng.rectTransform.rect.height / 2.0f;
            //}
        }
    }


    public void setNickName (string nickName) {
        mNickLabel.text = nickName;
    }

    public void setThumb (Texture2D texture) {
        mThumbPng.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
    }

    public void setThumb (string url) {
        StartCoroutine(LoadLocalImage(url));
    }

    IEnumerator LoadLocalImage (string url) {
        DebugConsole.Log("getting local image:" + url);
        WWW www = new WWW(url);
        yield return www;

        Texture2D texture = www.texture;
        Sprite m_sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
        mThumbPng.sprite = m_sprite;
    }

    public void setScore (string score) {
        mScoreLabel.text = score;
    }
}
