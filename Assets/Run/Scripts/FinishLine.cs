﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour {

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    private bool isTrue = true;

    public void Init () {
        isTrue = true;
    }

    private void OnTriggerEnter (Collider other) {
        if (other.tag == "Player") {
            if (isTrue == true) {
                //Global.GameManager.UiMgr.endWheel(
                //    () => Global.GameManager.onIntroDialogClosed(), 3f
                //);
                Global.GameManager.ReplayGame();
                isTrue = !isTrue;
            } else
                return;
        }
    }
}
