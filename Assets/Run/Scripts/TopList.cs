﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopList : MonoBehaviour {
    public List<TopItem> m_TopItems;
    public Transform m_ItemParent;
    public GameObject m_TopItem;
    List<TopRecord> topList;

    void Awake() {
        UserManager.Instance.OnTopUpdateChanged += this.updateList;
    }

    void Start() {
        m_ItemParent = this.transform.Find("Grid").transform;
    }

    //public override void onTopUpdate() {
    //    this.updateList();
    //}

    void updateList() {
        topList = UserManager.Instance.getTopList();

        for (int i = 0; i < topList.Count; i++) {
            TopRecord tr = topList[i];
            TopItem item = this.getItemByUserId(tr.getUserId());
            if (!item) {
                if (tr.getUserId() != 0) {
                    //item = new TopItem(tr);

                    GameObject obj = GameObject.Instantiate(m_TopItem);
                    obj.name = obj.name.Replace("(Clone)", tr.getUserId().ToString());
                    obj.transform.SetParent(m_ItemParent, false);
                    obj.SetActive(true);
                    item = obj.GetComponent<TopItem>();
                    item.setTopItem(tr);
                    m_TopItems.Add(item);
                } else {
                    return;
                }
            } else {
                item.transform.SetSiblingIndex(i);
                //TODO: 排行榜换位动画
                //float delta = targetPosition.y - item.getPosition().y;
                //if (delta < 0) {
                //    delta = -delta;
                //}

                //if (delta > POSITION_TORLERENCE) {
                //    item.stopAllActions();
                //    item.runAction(MoveTo::create(0.3, targetPosition));
                //}
            }
            item.setIndex(i + 1);

            item.setScore(tr.getScore());
        }
        this.removeDefeatedTop();
    }

    TopItem getItemByUserId(int userId) {
        List<TopItem> children = m_TopItems;
        for (int i = 0; i < children.Count; i++) {
            TopItem item = children[i];
            if (item.getUserId() == userId) {
                return item;
            }
        }
        return null;
    }

    void removeDefeatedTop() {
        List<TopItem> children = m_TopItems;
        List<TopItem> removingList = new List<TopItem>();
        for (int i = 0; i < children.Count; i++) {
            TopItem item = children[i];
            int userId = item.getUserId();
            bool isplay = UserManager.Instance.isTopPlayer(userId);
            if (!isplay) {
                removingList.Add(item);
            }
        }

        for (int i = 0; i < removingList.Count; i++) {
            removingList[i].gameObject.SetActive(false);
            Destroy(removingList[i]);
        }
    }

    public void clearItems() {
        List<TopItem> children = m_TopItems;
        for (int i = 0; i < children.Count; i++) {
            TopItem node = children[(i)];
            node.gameObject.SetActive(false);
            Destroy(node);
        }
    }
}