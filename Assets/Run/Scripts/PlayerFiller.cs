﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerFiller : MonoBehaviour {
    public Image mThumbPng;
    private float progreeVal;

    public RectTransform filler;
    public RectTransform fillbar;

    void Awake () {
        mThumbPng = transform.Find("Handle/Mask/ThumbPng").gameObject.GetComponent<Image>();
    }

    // Use this for initialization
    void Start () {
        filler = transform as RectTransform;
        fillbar = transform.Find("Fillbar") as RectTransform;
    }

    // Update is called once per frame
    void Update () {

    }

    public void SetProgressbarValue (float rProgress) {
        progreeVal = rProgress;
        //Image img = transform.GetComponent<Image>();
        //if (img != null) {
        //    float w = ((float)rProgress / 100) * (img.sprite.rect.width);
        //    img.rectTransform.sizeDelta = new Vector2(w, img.rectTransform.sizeDelta.y);
        //}

        float w = (100 - rProgress) / 100 * fillbar.rect.width;
        filler.sizeDelta = new Vector2(w, filler.rect.height);
    }

    public void setThumb (Texture2D texture) {
        mThumbPng.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
    }

    public void setThumb (string url) {
        StartCoroutine(LoadLocalImage(url));
    }

    IEnumerator LoadLocalImage (string url) {
        DebugConsole.Log("getting local image:" + url);
        WWW www = new WWW(url);
        yield return www;

        Texture2D texture = www.texture;
        Sprite m_sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
        mThumbPng.sprite = m_sprite;
    }
}
