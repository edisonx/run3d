﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitingList : MonoBehaviour {
    public List<WaitingItem> mWaitingItems;
    public Transform m_ItemParent;
    public GameObject m_WaitingItem;
    List<User> mUserList;

    void Awake() {
        UserManager.Instance.OnWaitingUpdateChanged += this.updateList;
    }

    void Start() {
        m_ItemParent = this.transform.Find("Grid").transform;
    }

    void updateList() {
        mUserList = UserManager.Instance.getWaitingUsers();

        Debug.Log(string.Format("WaitingList.update wait={0}", mUserList.Count));

        for (int i = 0; i < mUserList.Count; i++) {
            User user = mUserList[i];
            WaitingItem item = this.getItemBySocketId(user.SocketID);
            if (!item) {
                //item = new WaitingItem(user);

                GameObject obj = GameObject.Instantiate(m_WaitingItem);
                obj.name = obj.name.Replace("(Clone)", user.SocketID.ToString());
                obj.transform.SetParent(m_ItemParent, false);
                obj.SetActive(true);
                item = obj.GetComponent<WaitingItem>();
                item.setWaitingItem(user);
                mWaitingItems.Add(item);
            } else {
                item.transform.SetSiblingIndex(i);
                //TODO: 排行榜换位动画
                //item.stopAllActions();
                //item.runAction(MoveTo::create(0.3, targetPosition));
            }
            if (i >= UserManager.Instance.WAIT_USER_SIZE) {
                item.gameObject.SetActive(false);
            } else {
                item.gameObject.SetActive(true);
            }
        }
        removePopOrQuitUsers();
    }

    void removePopOrQuitUsers() {
        List<WaitingItem> children = mWaitingItems;
        List<WaitingItem> removingList = new List<WaitingItem>();
        for (int i = 0; i < children.Count; i++) {
            WaitingItem item = children[i];
            int userId = item.getSocketId();
            if (!UserManager.Instance.isWaiting(userId)) {
                mWaitingItems.Add(item);
            }
        }

        Debug.Log(string.Format("removing {0} wait users", removingList.Count));
        for (int i = 0; i < removingList.Count; i++) {
            removingList[i].gameObject.SetActive(false);
            Destroy(removingList[i]);
        }
    }

    WaitingItem getItemBySocketId(int Id) {
        List<WaitingItem> children = mWaitingItems;
        for (int i = 0; i < children.Count; i++) {
            WaitingItem item = children[i];
            if (item.getSocketId() == Id) {
                return item;
            }
        }
        return null;
    }
}