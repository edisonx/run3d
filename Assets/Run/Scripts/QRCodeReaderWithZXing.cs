﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using ZXing;
using System.IO;
using System.Threading;

/// <summary>
/// Lynn.C
/// 2017-1-4
/// QR code reader with Z xing.
/// </summary>
public class QRCodeReaderWithZXing : MonoBehaviour
{

	public Canvas canvas;
	public RawImage cameraTexture;
	public Image rectImg;
	private float timer = 0;
	public bool isScan = false;
	private Action<string> onScanedCallBack;

	//private WebCamTexture webCameraTexture;
	private BarcodeReader barcodeReader;
	//框选区域的取色数组
	private Color32[] data;
	private Texture2D tex2D;

	private int width, height;
	private int recognizeW, recognizeH;
	private static object locker = new object();
	private bool scanedSucceed = false;
	private string scanCode;
    private bool isRecognizing = false;

    IEnumerator Start ()
	{
		width = Screen.width;
		height = Screen.height;
		recognizeW = (int)(rectImg.rectTransform.rect.width * canvas.scaleFactor);
		recognizeH = (int)(rectImg.rectTransform.rect.height * canvas.scaleFactor);

		yield return Application.RequestUserAuthorization (UserAuthorization.WebCam | UserAuthorization.Microphone);
		//if (Application.platform == RuntimePlatform.WindowsPlayer) {
		//	cameraTexture.transform.localEulerAngles = new Vector3 (0, 180f, 0);
		//} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
		//	cameraTexture.transform.localEulerAngles = new Vector3 (0, 180f, 270f);
		//	cameraTexture.rectTransform.sizeDelta = new Vector2 (height, width);
		//} else if (Application.platform == RuntimePlatform.Android) {
		//	cameraTexture.transform.localEulerAngles = new Vector3 (0, 0, 270f);
		//	cameraTexture.rectTransform.sizeDelta = new Vector2 (height, width);
		//}
		barcodeReader = new BarcodeReader ();
		barcodeReader.AutoRotate = true;

		//WebCamDevice[] devices = WebCamTexture.devices;
		//      if (devices.Length <= 0)
		//      {
		//          this.enabled = false;
		//          yield return null;
		//      }

		//string devicename = devices [0].name;
		//webCameraTexture = new WebCamTexture (devicename, width, height);
		//cameraTexture.texture = webCameraTexture;
		//webCameraTexture.Play ();

		tex2D = new Texture2D (recognizeW, recognizeH, TextureFormat.RGB24, true);
	}

	void Update ()
	{
		//if (webCameraTexture != null) {
		//	cameraTexture.rectTransform.sizeDelta = GetSizeDelta (cameraTexture.rectTransform.sizeDelta.y, (float)webCameraTexture.width / webCameraTexture.height);
		//}
		if (isScan) {
			timer += Time.deltaTime;
			if (timer > 0.5f&& !isRecognizing) {
				StartCoroutine (ScanQRCode ());
				timer = 0;
			}
		}else if(scanedSucceed)
		{
			scanedSucceed = false;
			if (onScanedCallBack != null) onScanedCallBack(scanCode);
			scanCode = string.Empty;
		}

	}

	/// <summary>
	/// Scans the QR code.
	/// 开始扫描
	/// </summary>
	/// <returns>The QR code.</returns>
	IEnumerator ScanQRCode()
	{
		yield return new WaitForEndOfFrame();

        Vector2 screenPoint = UIUtil.GetUIScreenPos(rectImg.transform) ;
//#if !UNITY_EDITOR
//        screenPoint.y = 1 - screenPoint.y;
//#endif
        Debug.Log(screenPoint);
        //screenPoint = rectImg.rectTransform.position;
        float tWidth,tHeight;
		tHeight = screenPoint.y - rectImg.rectTransform.rect.height * canvas.scaleFactor / 2;
		tWidth = screenPoint.x - rectImg.rectTransform.rect.width * canvas.scaleFactor / 2;
        tex2D.ReadPixels (new Rect (tWidth, 
			tHeight,
			recognizeW,
			recognizeH), 0, 0);
		tex2D.Apply (false);

        lock (locker)
		{
			data = tex2D.GetPixels32();
		}
        isRecognizing = true;
        ThreadPool.QueueUserWorkItem(DecodeQR);

		//WriteTexture (tex2D);

		RenderTexture.active = null;

	}

	void WriteTexture(Texture2D tex2D)
	{
		byte[] bytes = tex2D.EncodeToPNG ();
		//获取系统时间
		System.DateTime now = new System.DateTime();
		now = System.DateTime.Now;
		string filename = string.Format("image{0}{1}{2}{3}.png", now.Day, now.Hour, now.Minute, now.Second);
		//Debug.Log (Application.dataPath + "/../" + filename);
        if(!System.IO.Directory.Exists(Application.persistentDataPath + "/ScreenShot/"))
        {
            System.IO.Directory.CreateDirectory(Application.persistentDataPath + "/ScreenShot/");
        }
        File.WriteAllBytes (Application.persistentDataPath + "/ScreenShot/" + filename, bytes);
	}

	void DecodeQR(object obj)
	{
		lock (locker)
		{
			int w = recognizeW;
			int h = recognizeH;
            var br = barcodeReader.Decode(data, w, h);
            isRecognizing = false;
            if (br != null)
			{
				Debug.Log(br.Text);
				scanedSucceed = true;
				scanCode = br.Text;
				isScan = false;
			}
		}
	}

	/// <summary>
	/// Gets the size delta.
	/// 缩放后的sizedelta
	/// </summary>
	/// <returns>The size delta.</returns>
	/// <param name="matchValue">Match value.</param>
	/// <param name="proportion">Proportion.</param>
	Vector2 GetSizeDelta(float matchValue, float proportion)
	{
		Vector2 sizeDelta;
		sizeDelta = new Vector2 (matchValue * proportion, matchValue);
		return sizeDelta;
	}


	public void SetOnScanedCallBack(Action<string> cb)
	{
		onScanedCallBack = cb;
	}

}
