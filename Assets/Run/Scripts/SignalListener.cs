﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Threading;
using UnityEngine;

public class SignalListener : MonoBehaviour {
#if UNITY_STANDALONE_LINUX_API
    //pthread_rwlock_t mRWlock;
#endif
    private static SignalListener _instance = null;
    private StatusDelegate mDelegate;
    private ReaderWriterLock _rwlock;
    private bool mGameStartingSignal;
    private long mSenderTimeStamp;
    private float mAfterTime;

    public static SignalListener Instance {
        get {
            return _instance;
        }
    }

    private SignalListener() {
        mDelegate = new StatusDelegate();
#if UNITY_STANDALONE_LINUX_API
        //mRWlock = PTHREAD_RWLOCK_INITIALIZER;
#endif
        _rwlock = new ReaderWriterLock();
        mGameStartingSignal = false;
        mSenderTimeStamp = 0;
        mAfterTime = 0;

        initMailWaitor();
    }

    public void setDelegate(StatusDelegate delegator) {
        mDelegate = delegator;
    }

#if UNITY_STANDALONE_LINUX_API
    [DllImport("libc.so.6")]
    private static extern int msgget(int key, Int32 msgflg);

    [DllImport("libc.so.6")]
    private static extern uint msgrcv(Int32 msqid, IntPtr msgp, uint msgsz, Int32 msgtyp, Int32 msgflg);

    [DllImport("libc.so.6")]
    private static extern int __errno_location();

#endif

    public void initMailWaitor() {

    }

    static void waitMailLoop() {
    }

    void setGameStartingSignalInSubThread(float afterSeconds, long timeStamp) {
#if UNITY_STANDALONE_LINUX
        //pthread_rwlock_wrlock(mRWlock);
#endif
        mGameStartingSignal = true;
        mSenderTimeStamp = timeStamp;
        mAfterTime = afterSeconds;
#if UNITY_STANDALONE_LINUX
        //pthread_rwlock_unlock(mRWlock);
#endif
    }

    public void checkGameStartingSignalInMainThread() {
        bool signalValue = false;
        long senderTimeStamp = 0;
        float afterTime = 0;

#if UNITY_STANDALONE_LINUX
        //pthread_rwlock_rdlock(mRWlock);
#endif
        if (mGameStartingSignal) {
            signalValue = true;
            mGameStartingSignal = false;
            senderTimeStamp = mSenderTimeStamp;
            afterTime = mAfterTime;
        }
#if UNITY_STANDALONE_LINUX
        //pthread_rwlock_unlock(mRWlock);
#endif

        if (signalValue && mDelegate) {
            //long curTime = Utility.GetTimeStamp(false);
            //float targetValue = afterTime - (curTime - senderTimeStamp) * 0.001 + ConfigCenter.Instance.timeOffset();
            //if (targetValue <= 0.0) {
            //    targetValue = 0.0f;
            //}
            //mDelegate.onGameStarting(targetValue, ConfigCenter.Instance.isSignalFirstRoundLock());
        }
    }

    void Awake() {
#if YL_DEBUG
        InitConsole();
#endif
    }

#if YL_DEBUG
    void InitConsole() {
        string rName = "DebugConsloe";
        GameObject consloe = GameObject.Find(rName);
        if (consloe == null) {
            consloe = new GameObject(rName);
            consloe.name = rName;
        }
        var com = consloe.GetComponentSafe<DebugConsole>();
        com.IsDebugMode = true;
        com.isOpenBriefView = true;
    }
#endif

#if UNITY_STANDALONE_LINUX
    void Start() {
        Test();
    }

    public void Test() {
        DebugConsole.Log("Start testThread ...");
        StartCoroutine(testThread());
        DebugConsole.Log("testThread Over !!!");
    }

    int MSG_QUEUE_ID = 1234;

    struct msg_st {
        public float after;
        public long timestamp;
    }

    IEnumerator testThread() {
        int msgid;

        try {
            msgid = msgget(MSG_QUEUE_ID, 0666 | 01000);
        } catch (Exception e) {
            DebugConsole.LogError(string.Format("msgget : {0}", e));
            throw;
        }
        DebugConsole.Log("msgid = " + msgid);

        if (msgid == -1) {
            DebugConsole.LogError(string.Format("msgget failed with error: {0}", __errno_location()));
            yield break;
        }

        msg_st data = new msg_st() {
            after = 0,
            timestamp = 0
        };

        while (true) {
            try {
                int msgsz = Marshal.SizeOf(data);
                IntPtr msgp = Marshal.AllocHGlobal(msgsz);
                int len = (int)msgrcv(msgid, msgp, (uint)msgsz, 0, 0);
                if (-1 == len) {
                    DebugConsole.LogError(string.Format("msgrcv failed with errno: {0} , {1}", len, __errno_location()));
                    yield break;
                }
                object obj = Marshal.PtrToStructure(msgp, typeof(msg_st));
                data = (msg_st)obj;
                DebugConsole.Log(string.Format("recieved wrote: {0} | {1}", data.after, data.timestamp));
                if (data.timestamp > 0) {

                }
                setGameStartingSignalInSubThread(data.after, data.timestamp);
            } catch (Exception e) {
                DebugConsole.LogError(string.Format("msgrcv : {0}", e));
                throw;
            }
            DebugConsole.Log("WaitForEndOfFrame！");
            yield return new WaitForSeconds(1f);
        }
    }
#endif
}
