﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartUp : MonoBehaviour {
    int scWidth = Screen.width;
    int scHeight = Screen.height;
    string[] cmdArgs = System.Environment.GetCommandLineArgs();

    void Awake () {

    }

    void Start () {
        _GameController.Instance.Initialize(Startup);
        Application.targetFrameRate = 30;       //固定帧数
    }

    void Startup () {
        //LShapClient.Instance.Initialize();

        DebugConsole.Log("[Framework initialize complete]");
        DebugConsole.Log("[Assetbundle prepare complete]");
        DebugConsole.Log("[LShape initialize complete]");
        DebugConsole.Log("[StreamingAssetsPath]：" + Application.streamingAssetsPath);
        DebugConsole.Log("[RuntimeAssetsPath] :" + AppPlatform.RuntimeAssetsPath);

        //程序初始化完毕， 进入游戏
        try {
            var maintick = Global.MainTick;
            StartCoroutine(StartupSocket());
        } catch (Exception e) {
            DebugConsole.Log(e.Message + e.StackTrace);
        }
    }

    IEnumerator StartupSocket () {
        yield return new WaitForEndOfFrame();
        Global.MainTick.gameObject.AddComponent<SignalListener>();
        //Destroy(gameObject);
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
        if (cmdArgs.Length >= 2) {
            string[] windowArgs = cmdArgs[1].Split('@');
            if (windowArgs.Length >= 4) {
                gameObject.AddComponent<WindowMod>().SetWindowPos(int.Parse(windowArgs[0]), int.Parse(windowArgs[1]), int.Parse(windowArgs[2]), int.Parse(windowArgs[3]));
            }
        }
#else
        int designWidth = (int)Global.ReferenceResolution.x; //这个是设计分辨率
        int designHeight = (int)Global.ReferenceResolution.y;

        //if (scWidth <= designWidth || scHeight <= designHeight)
        //    return;
        Screen.SetResolution(designWidth, designHeight, true);//设置分辨率全屏
#endif
    }
}
