﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnim : MonoBehaviour {
    public Vector3 offset = new Vector3(-3.5f, 3.5f, -4);//相机相对于玩家的位置
    private Transform target;
    private Vector3 pos;

    public float speed = 2;

    public Vector3 initPos = new Vector3(-100, 11.5f, -20.1f);

    // Use this for initialization
    void Start() {
        initPos = this.transform.position;
    }

    public void Init() {
        this.transform.position = initPos;
    }

    // Update is called once per frame
    void LateUpdate() {
        GameObject go = Global.GameManager.getFirstPlayerGO();
        if (go != null) {
            target = go.transform;
            pos = target.position + offset;

            //if (Vector3.Distance(new Vector3(Global.GameManager.targetObj.transform.position.x, transform.position.y, transform.position.z),
            //        new Vector3(pos.x, transform.position.y, transform.position.z)) > 0) {
            this.transform.position = Vector3.Lerp(this.transform.position, new Vector3(pos.x, offset.y, offset.z), speed * Time.deltaTime); //调整相机与玩家之间的距离 目前只跟随X轴向
            //}
            //else {
            //    //Quaternion angel = Quaternion.LookRotation(target.position - this.transform.position);              //获取旋转角度
            //    //this.transform.rotation = Quaternion.Slerp(this.transform.rotation, angel, speed * Time.deltaTime);
            //    this.transform.LookAt(target);
            //}

            //Global.GameManager.UiMgr.ShowDistance(go.transform);

            //this.transform.position = Vector3.Lerp(this.transform.position, pos, speed * Time.deltaTime);       //调整相机与玩家之间的距离
            //Quaternion angel = Quaternion.LookRotation(target.position - this.transform.position);              //获取旋转角度
            //this.transform.rotation = Quaternion.Slerp(this.transform.rotation, angel, speed * Time.deltaTime);
        }
    }
}
