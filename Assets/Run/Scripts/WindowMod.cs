﻿
using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Diagnostics;
using UnityEngine;

public class WindowMod : MonoBehaviour {
    public Rect screenPosition;

    // 在这里设置你想要的窗口宽
    int _Txtwith = 1280;

    // 在这里设置你想要的窗口高
    int _Txtheight = 1088;

    [DllImport("user32.dll")]
    static extern IntPtr SetWindowLong (IntPtr hwnd, int _nIndex, int dwNewLong);

    [DllImport("user32.dll")]
    static extern bool SetWindowPos (IntPtr hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

    [DllImport("user32.dll")]
    static extern IntPtr GetForegroundWindow (); //获取最前端窗体句柄

    [DllImport("user32.dll")]
    static extern bool ShowWindow (IntPtr hWnd, int nCmdShow);

    //[DllImport("user32.dll")]
    //static extern IntPtr GetWindowThreadProcessld ();

    // not used rigth now
    //const uint SWP_NOMOVE = 0x2;
    //const uint SWP_NOSIZE = 1;
    //const uint SWP_NOZORDER = 0x4;
    //const uint SWP_HIDEWINDOW = 0x0080;
    const uint SWP_SHOWWINDOW = 0x0040;
    const int GWL_STYLE = -16; //边框用的
    const int WS_BORDER = 1;

    private int borderX = -3; //用于计算排除窗口边框的X轴向的多余位置
    private int borderY = -26; //用于计算排除窗口边框的Y轴向的多余位置

    public void SetWindowPos (int x, int y, int w, int h) {
        screenPosition.x = x;
        screenPosition.y = y;
        screenPosition.width = w;
        screenPosition.height = h;
    }

    public void SetResolution (int width, int height, bool fullscreen) {
        _Txtwith = width;
        _Txtheight = height;
    }

    void Start () {
        /*screenPosition.x = (int)((Screen.currentResolution.width - screenPosition.width) / 2);
        screenPosition.y = (int)((Screen.currentResolution.height - screenPosition.height) / 2);
        if (Screen.currentResolution.height <= 768) {
            screenPosition.y = 0;
        }*/

        StartCoroutine(Setposition());
        //Screen.SetResolution(_Txtwith, _Txtheight, false);		//这个是Unity里的设置屏幕大小，
    }

    IEnumerator Setposition () {
        yield return new WaitForSeconds(0.1f); //不知道为什么发布于行后，设置位置的不会生效，我延迟0.1秒就可以
        //SetWindowLong(GetForegroundWindow(), GWL_STYLE, WS_BORDER);     //无边框
        bool result = SetWindowPos(GetForegroundWindow(), -1, (int)screenPosition.x + borderX,
            (int)screenPosition.y + borderY, (int)screenPosition.width, (int)screenPosition.height,
            SWP_SHOWWINDOW); //设置屏幕大小和位置
    }
}