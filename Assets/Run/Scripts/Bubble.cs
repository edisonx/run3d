﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble : MonoBehaviour {

    public enum BubbleState
    {
        Move,
        Brokening,
        Broken
    }
    public BubbleState state = BubbleState.Broken;
    public int type = -1;
    public int quarant = 1;
    // Use this for initialization
    private Vector3 velocity;
    private float acceleration;
    private float factor = 0.00001f;
    public float rangeRadius;
    public float rangeY;
    private float scaleDelta = 0.001f;
    private float crtScale = 0f;
    private GameObject borkenEffect;
    private float radius = 0.26f;
    public bool activeable = true;

    void Start () {
        //Rest();
    }

    public void  Rest()
    {
        if(borkenEffect == null)
        {
            borkenEffect = transform.Find("Effect_QiPao_posui").gameObject;
        }
       

        if (state != BubbleState.Broken)
            state = BubbleState.Broken;
        velocity = new Vector3(Random.Range(-0.2f,0.2f), Random.Range(0.15f, 0.2f), Random.Range(-0.2f, 0.2f));
        float acc = Random.Range(0.005f, 0.008f);
        acceleration = acc;
        factor = Random.Range(0.02f, 0.05f);
        transform.localScale = Vector3.zero;
        scaleDelta = Random.Range(0.01f, 0.03f);
        crtScale = 0f;
        state = BubbleState.Move;

        ShowBrokenEffect(false);
    }
 
    public void Stop()
    {
        state = BubbleState.Broken;
    }

    // Update is called once per frame
    void Update () {
        if (state == BubbleState.Broken || state == BubbleState.Brokening) return;
        if(transform.localPosition.y >= rangeY)
        {
            Broken();
            return;
        }

        //velocity += acceleration * velocity;
        Vector3 pos = transform.localPosition;
        pos += velocity * factor;
        transform.localPosition = pos;
        float dis = Vector3.Distance(Vector3.zero, new Vector3(pos.x, 0, pos.z));
        if(dis >= rangeRadius * 0.8f)
        {
            velocity.x = -velocity.x;
            velocity.z = -velocity.z;
            //pause = true;
        }

        if(crtScale >= 1)
        {
            crtScale = 1;
            transform.localScale = Vector3.one;
        }
        else
        {
            crtScale += scaleDelta;
            Vector3 scale = transform.localScale;
            scale.Set(crtScale, crtScale, crtScale);
            transform.localScale = scale;
        }
    }


    public void Broken()
    {
        //todo paly borken effect
        state = BubbleState.Brokening;
        ShowBrokenEffect(true);
        StartCoroutine(BrokenEffectEnd());
    }

    void ShowBrokenEffect(bool isShow)
    {
        if (borkenEffect == null)
        {
            print(gameObject.name);
        }
        borkenEffect.gameObject.SetActive(isShow);
        int count = transform.childCount;
        Transform child;
        while (count > 0)
        {
            child = transform.GetChild(count - 1);
            if (child.gameObject != borkenEffect)
            {
                child.gameObject.SetActive(!isShow);
            }
            count--;
        }
    }

    IEnumerator BrokenEffectEnd()
    {
        yield return new WaitForSeconds(1f);
        state = BubbleState.Broken;
    }

}
