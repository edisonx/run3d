﻿using System.Collections;
using System.Collections.Generic;

public class TopRecord {
    int mUserId;
    string mNickName;
    string mThumbUrl;
    int mScore;

    public TopRecord(int userId, string nickName, string thumbUrl, int score) {
        if (thumbUrl.Length > 0) {
            mThumbUrl = thumbUrl;
        } else { //默认头像
            mThumbUrl = "http://t.cn/Ru4c6rJ";
        }

        if (nickName.Length > 0) {
            mNickName = nickName;
        } else {
            string buff = "RunMan:" + UserManager.Instance.gDefaultNameIndex++;
            mNickName = buff;
        }
        mScore = score;
        mUserId = userId;
    }

    public int getUserId() {
        return mUserId;
    }

    public int getScore() {
        return mScore;
    }

    public void setScore(int score) {
        mScore = score;
    }

    public string getNickName() {
        return mNickName;
    }

    public string getThumbUrl() {
        return mThumbUrl;
    }
}
