﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility {

    public static Vector2 string2Vec2(string str) {
        Vector2 vec = Vector2.zero;
        char needle = ',';
        string[] result = str.Split(needle);
        if (result.Length > 1) {
            float x = float.Parse(result[0]);
            float y = float.Parse(result[1]);

            vec = new Vector2(x, y);

            DebugConsole.Log(string.Format("[Vector2] = ({0}, {1})", x.ToString(), y.ToString()));
        }
        return vec;
    }
}
