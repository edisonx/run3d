#!/usr/bin/env python
# -*- coding:UTF-8 -*-
import sys
import time
import threading
import random

sys.path.append("libs")

SCREEN_ID = 1261
USERS_NUMBER = 15

global TESTER_INDEX
TESTER_INDEX = 0

#url = 'ws://gate0.edisonx.cn:2380'
#url = 'ws://h5.edisonx.cn:2380'
#url = 'ws://www.orchidstudio.cn:2360'
url = 'wss://miniapp.edisonx.cn/run:3360'

import websocket
try:
    import thread
except ImportError:
    import _thread as thread
    import time

def on_message(ws, message):
    print('message:' + message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")



def getInitMsg():
    thumbImgs = ("http://touxiang.qqzhi.com/uploads/2012-11/1111004647841.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111014046331.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111021813342.png", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111112015966.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111032724256.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111105437504.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111122150831.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111122549416.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111020503617.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111111326568.jpg", \
        "http://www.qqzhi.com/uploadpic/2015-01-14/040518738.jpg")
    global TESTER_INDEX
    TESTER_INDEX += 1
    if(TESTER_INDEX >= len(thumbImgs)):
        TESTER_INDEX = 0
    return ("prof_c;" + str(SCREEN_ID) + ";����." + str(TESTER_INDEX) + \
            ";" + thumbImgs[TESTER_INDEX] + ";" + str(TESTER_INDEX)) + ";" + str(TESTER_INDEX)

def on_open(ws):

    def run(*args):
        time.sleep(1)
        m0 = getInitMsg()
        ws.send(m0)
        print ('data:' + m0)
        time.sleep(1)
        ws.send("f")

        try:
            for j in range(100):
                # for i in range(60):
                #     time.sleep(.12)
                #     random.seed()
                #     x = random.randint(-99,99)
                #     y = random.randint(-99,99)
                #     ws.send("mv:" + str(x) + "," + str(y))
                time.sleep(1)
                # if j%5 == 3:
                b = random.randint(1, 6)
                ws.send("bst:" + str(b))

        except Exception, e:
            return

        ws.close()
        print("thread terminating...")

    thread.start_new_thread(run, ())


def oneConnection():
    websocket.enableTrace(False)
    ws = websocket.WebSocketApp(url, on_message = on_message, on_error = on_error, on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()

for i in range(USERS_NUMBER):
    thrd = threading.Thread(target=oneConnection, args=())
    thrd.setDaemon(True)
    thrd.start()

    time.sleep(1)

    print "user connecting NO.", i

time.sleep(3600*24)