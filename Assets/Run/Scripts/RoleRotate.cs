﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RoleRotate : MonoBehaviour
{
    public Transform RoleModelPos;
    private RawImage RoleImage;
    private float roleModel_Y = 0;

    // Use this for initialization
    void Start()
    {
        RoleImage = transform.GetComponent<RawImage>();
        EventTriggerListener.Get(RoleImage.gameObject).SetEventHandle(EnumTouchEventType.OnDrag, RoleModelDrag);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void RoleModelDrag(GameObject handle, object _obj)
    {
        if (RoleModelPos != null)
        {
            var roleVar = (PointerEventData)_obj;
            Vector3 roleVec = roleVar.delta;
            roleModel_Y -= roleVec.x;
            RoleModelPos.localRotation = Quaternion.Euler(0, roleModel_Y, 0);
        }
    }
}
