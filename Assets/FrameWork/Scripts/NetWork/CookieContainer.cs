﻿using System;
using System.Collections.Generic;

public class CookieContainer {

	Dictionary<string, HttpCookie> _cookies = new Dictionary<string, HttpCookie>();

	/// <summary>
	/// Clear this instance.
	/// </summary>
	public void Clear()
	{
		_cookies.Clear ();
	}

	/// <summary>
	/// Contains the specified uri.
	/// </summary>
	/// <param name="uri">URI.</param>
	public bool Contains(Uri uri)
	{
		return _cookies.ContainsKey (uri.Host);
	}

	/// <summary>
	/// Contains the specified uri.
	/// </summary>
	/// <param name="uri">URI.</param>
	public bool Contains(string uri)
	{
		return Contains (new Uri (uri));
	}

	/// <summary>
	/// Get the specified uri.
	/// </summary>
	/// <param name="uri">URI.</param>
	public HttpCookie Get(Uri uri)
	{
		return _cookies [uri.Host];
	}

	/// <summary>
	/// Get the specified uri.
	/// </summary>
	/// <param name="uri">URI.</param>
	public HttpCookie Get(string uri)
	{
		return Get (new Uri (uri));
	}

	/// <summary>
	/// Set the specified uri, HttpCookie and clearExisting.
	/// </summary>
	/// <param name="uri">URI.</param>
	/// <param name="Http_Cookie">HttpCookie.</param>
	/// <param name="clearExisting">If set to <c>true</c> clear existing.</param>
	public void Set(Uri uri, HttpCookie Http_Cookie, bool clearExisting = false)
	{
		string host = uri.Host;
		if (_cookies.ContainsKey (host))
			_cookies [host].Set (Http_Cookie, clearExisting);
		else
			_cookies.Add (host, Http_Cookie);
	}

	/// <summary>
	/// Set the specified uri, HttpCookie and clearExisting.
	/// </summary>
	/// <param name="uri">URI.</param>
	/// <param name="Http_Cookie">HttpCookie.</param>
	/// <param name="clearExisting">If set to <c>true</c> clear existing.</param>
	public void Set(string uri, HttpCookie Http_Cookie, bool clearExisting = false)
	{
		Set(uri, Http_Cookie, clearExisting);
	}

	/// <summary>
	/// Set the specified uri, cookieString and clearExisting.
	/// </summary>
	/// <param name="uri">URI.</param>
	/// <param name="cookieString">HttpCookie string.</param>
	/// <param name="clearExisting">If set to <c>true</c> clear existing.</param>
	public void Set(Uri uri, string cookieString, bool clearExisting = false)
	{
		string host = uri.Host;
		if (_cookies.ContainsKey(host))
			_cookies[host].Set(cookieString, clearExisting);
		else
			_cookies.Add(host, new HttpCookie(cookieString));
	}

	/// <summary>
	/// Set the specified uri, cookieString and clearExisting.
	/// </summary>
	/// <param name="uri">URI.</param>
	/// <param name="cookieString">HttpCookie string.</param>
	/// <param name="clearExisting">If set to <c>true</c> clear existing.</param>
	public void Set(string uri, string cookieString, bool clearExisting = false)
	{
		Set(new Uri(uri), cookieString, clearExisting);
	}

	/// <summary>
	/// Set the specified uri, name, value and clearExisting.
	/// </summary>
	/// <param name="uri">URI.</param>
	/// <param name="name">Name.</param>
	/// <param name="value">Value.</param>
	/// <param name="clearExisting">If set to <c>true</c> clear existing.</param>
	public void Set(Uri uri, string name, string value, bool clearExisting = false)
	{
		string host = uri.Host;
		if (_cookies.ContainsKey(host))
			_cookies[host].Set(name, value);
		else
		{
			HttpCookie Http_Cookie = new HttpCookie();
			Http_Cookie.Set(name, value);
			_cookies.Add(host, Http_Cookie);
		}
	}

	/// <summary>
	/// Set the specified uri, name, value and clearExisting.
	/// </summary>
	/// <param name="uri">URI.</param>
	/// <param name="name">Name.</param>
	/// <param name="value">Value.</param>
	/// <param name="clearExisting">If set to <c>true</c> clear existing.</param>
	public void Set(string uri, string name, string value, bool clearExisting = false)
	{
		Set(new Uri(uri), name, value, clearExisting);
	}

	/// <summary>
	/// Gets or sets the <see cref="CookieContainer"/> with the specified uri.
	/// </summary>
	/// <param name="uri">URI.</param>
	public HttpCookie this[Uri uri]
	{
		get
		{
			return Get(uri);
		}
		set
		{
			Set(uri, value);
		}
	}

	/// <summary>
	/// Gets or sets the <see cref="CookieContainer"/> with the specified uri.
	/// </summary>
	/// <param name="uri">URI.</param>
	public HttpCookie this[string uri]
	{
		get
		{
			return Get(uri);
		}
		set
		{
			Set(uri, value);
		}
	}

}
