﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using LitJson;
using System.Text;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

public class NetWorkMananger : TSingleton<NetWorkMananger> {
    private string httpHost;
    private List<string> noneParamsGetAPIList; //无参数get方法API列表

    private NetWorkMananger() {
        ServicePointManager.ServerCertificateValidationCallback = TrustCertificate;
        noneParamsGetAPIList = new List<string>();
#if YL_TEST_ENV
        string host = Global.ServerConfig.GetString("Server", "debugHost") ;
#else
        string host = Global.ServerConfig.GetString("Server", "host");
#endif
        SetHttpHost(host);
        InitHeaders();
    }

    private static bool TrustCertificate(object sender, X509Certificate x509Certificate, X509Chain x509Chain, SslPolicyErrors sslPolicyErrors) {
        // all Certificates are accepted
        return true;
    }

    void InitHeaders() {
        string token = PlayerPrefs.GetString("Token");
        string uid = PlayerPrefs.GetString("UID");
        UnityWebClientSingleton.Instance.SetHeader("X-AUTH-TOKEN", token);
        UnityWebClientSingleton.Instance.SetHeader("X-UID", uid);
        string channel = "";
        string platform = "";
#if UNITY_ANDROID
        AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
        channel = jo.Call<string>("getChannelValue");
        platform = "android";
#elif UNITY_IOS
        channel = "ios";
        platform = "iphone";
#endif
        Debug.Log("channel:" + channel);
        UnityWebClientSingleton.Instance.SetHeader("X-PLATFORM", platform);
        UnityWebClientSingleton.Instance.SetHeader("X-CHANNEL-ID", channel);
        UnityWebClientSingleton.Instance.SetHeader("X-CLIENT-VERSION", Application.version);
        UnityWebClientSingleton.Instance.SetHeader("X-MACHINE-ID", SystemInfo.deviceUniqueIdentifier);

    }

    public bool NetWorkAvailable() {
        return Application.internetReachability != NetworkReachability.NotReachable;
    }

    public void SetHttpHost(string host) {
        httpHost = host;
        if (!httpHost.EndsWith("/")) {
            httpHost += "/";
        }
    }

    public void SendHttpRequest(string api, string method, string body) {
        Debug.Log(string.Format("request server\tapi:{0}\tmethod:{1}\tbody{2}", api, method, body));
        Dictionary<string, object> bodyDict = MiniJSON.Deserialize(body) as Dictionary<string, object>;
        string url = JointHttpUrl(api, method, bodyDict);
        Debug.Log(url);
        if (method == UnityWebRequest.kHttpVerbGET || method == UnityWebRequest.kHttpVerbDELETE) {
            UnityWebClientSingleton.Instance.Get(url, OnHttpReponse);
        } else if (method == UnityWebRequest.kHttpVerbPOST) {
            UnityWebClientSingleton.Instance.Post(url, body, OnHttpReponse);
        } else if (method == UnityWebRequest.kHttpVerbPUT) {
            UnityWebClientSingleton.Instance.Put(url, body, OnHttpReponse);
        } else {
            Debug.Log(string.Format("can not find request method:{0}", method));
        }

    }

    public void SendHttpRequest(string api, byte[] data, System.Action<string> onRes = null, System.Action onFail = null) {
        string url = httpHost + api;
        UnityWebClientSingleton.Instance.Post(url, data, (req) => {
            var resCode = req.responseCode;
            if (resCode >= 200 && resCode < 300) //succees
            {
                var resBody = req.downloadHandler.text;
                if (onRes != null) onRes(resBody);
            } else //fail
              {
                if (onFail != null) onFail();
            }
        });
    }

    private void OnHttpReponse(UnityWebRequest req) {
        var resCode = req.responseCode;
        string api = req.url;
        api = api.Replace(httpHost, "");
        if (req.method == UnityWebRequest.kHttpVerbGET || req.method == UnityWebRequest.kHttpVerbDELETE) {
            if (!noneParamsGetAPIList.Contains(api)) {
                int index = api.LastIndexOf('/');
                if (index >= 0) {
                    api = api.Substring(0, index);
                }
            }

        }

        Debug.Log("api:" + api + ",code:" + resCode);
        if (resCode >= 200 && resCode < 300) //succees
        {
            var resBody = req.downloadHandler.text;
            NetworkControler.Instance.OnHttpSuccees(api, resBody);
        } else //fail
        {
            NetworkControler.Instance.OnHttpFail(api, (int)resCode);
        }
    }

    private string JointHttpUrl(string api, string method, Dictionary<string, object> bodyDict) {
        StringBuilder sb = new StringBuilder(httpHost);
        sb.AppendFormat("{0}", api);
        if (method == UnityWebRequest.kHttpVerbGET || method == UnityWebRequest.kHttpVerbDELETE) {
            if (bodyDict != null && bodyDict.Count > 0) {
                sb.Append("/");
                var enu = bodyDict.GetEnumerator();
                if (bodyDict.Count == 1) {
                    while (enu.MoveNext()) {
                        sb.Append(enu.Current.Value);
                    }
                } else {
                    sb.Append('?');
                    while (enu.MoveNext()) {
                        if (enu.Current.Value.GetType() != typeof(string)) continue;
                        sb.AppendFormat("{0}={1}&", enu.Current.Key, enu.Current.Value.ToString());
                    }
                }
            } else {
                if (!noneParamsGetAPIList.Contains(api)) {
                    noneParamsGetAPIList.Add(api);
                }
            }
        }
        return sb.ToString();
    }
}
