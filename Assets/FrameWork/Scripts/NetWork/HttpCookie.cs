﻿using System.Collections;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// HttpCookie.
/// The HttpCookie class gets and sets properties of cookies.
/// </summary>
public class HttpCookie : Dictionary<string, string> {

    public HttpCookie() : base() { }

    public HttpCookie(IDictionary<string, string> cookies) : base(cookies) { }

    public HttpCookie(string cookieString) : base() {
        Set(cookieString);
    }

    /// <summary>
    /// Get the specified name.
    /// </summary>
    /// <param name="name">Name.</param>
    public string Get(string name) {
        return this[name];
    }

    /// <summary>
    /// Set the specified name, value and clearExisting.
    /// </summary>
    /// <param name="name">Name.</param>
    /// <param name="value">Value.</param>
    /// <param name="clearExisting">If set to <c>true</c> clear existing.</param>
    public void Set(string name, string value, bool clearExisting = false) {
        if (ContainsKey(name))
            this[name] = value;
        else
            Add(name, value);
    }

    /// <summary>
    /// Set the specified cookies and clearExisting.
    /// </summary>
    /// <param name="cookies">Cookies.</param>
    /// <param name="clearExisting">If set to <c>true</c> clear existing.</param>
    public void Set(IDictionary<string, string> cookies, bool clearExisting = false) {
        if (clearExisting)
            Clear();
        foreach (var cookie in cookies) {
            Set(cookie.Key, cookie.Value);
        }
    }

    /// <summary>
    /// Set the specified cookieString and clearExisting.
    /// </summary>
    /// <param name="cookieString">HttpCookie string.</param>
    /// <param name="clearExisting">If set to <c>true</c> clear existing.</param>
    public void Set(string cookieString, bool clearExisting = false) {
        if (clearExisting)
            Clear();
        if (string.IsNullOrEmpty(cookieString))
            return;
        string[] cookie_components = cookieString.Split(';');
        foreach (var kv in cookie_components) {
            int pos = kv.IndexOf('=');
            if (pos == -1)
                continue;
            else {
                string key = kv.Substring(0, pos);
                string val = kv.Substring(pos + 1);
                Set(key, val);
            }
        }
    }

    /// <summary>
    /// Returns a <see cref="System.String"/> that represents the current <see cref="HttpCookie"/>.
    /// </summary>
    /// <returns>A <see cref="System.String"/> that represents the current <see cref="HttpCookie"/>.</returns>
    public override string ToString() {
        StringBuilder builder = new StringBuilder();
        foreach (var kv in this) {
            builder.AppendFormat("{0}={1};", kv.Key, kv.Value);
        }
        return builder.ToString();
    }

}
