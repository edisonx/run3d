﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.IO;

public class ResourceManager
{
    public enum RequestResultCode
    {
        Succees = 1,
        Fail = -100,

    }
    private class ManagedResource
    {
        public string name;
        public string abName;
        public UnityEngine.Object resourceObject;
        public uint refCount = 0;
        public ManagedResource()
        {

        }
    }

    private static ResourceManager _instance;

    private Dictionary<string, ManagedResource> resources;
    private Dictionary<string, string> resourceConfig;
    private Dictionary<string, LoadResourceTask> loadingTaskDict;

    private ResourceManager()
    {
        resourceConfig = new Dictionary<string, string>();
        resources = new Dictionary<string, ManagedResource>();
        loadingTaskDict = new Dictionary<string, LoadResourceTask>();
    }

    /// <summary>
    /// 同步请求某个资源
    /// </summary>
    /// <param name="res"></param>
    /// <returns></returns>
    public bool RequestResource(string res)
    {
        if(string.IsNullOrEmpty(res) || !resourceConfig.ContainsKey(res))
        {
            DebugConsole.Log(string.Format("can not find resourece config!name:{0}", res));
            return false;
        }

        return true;
    }

    /// <summary>
    /// 同步请求一批次资源
    /// </summary>
    /// <param name="resList"></param>
    /// <returns></returns>
    public bool RequestBatchResource(List<string> resList)
    {
        return false;
    }

    /// <summary>
    /// 异步请求某个资源
    /// </summary>
    /// <param name="res"></param>
    /// <param name="callback"></param>
    public void RequeseResourceAsync(string res,Action<int> callback)
    {
        if(string.IsNullOrEmpty(res) || !resourceConfig.ContainsKey(res))
        {
            DebugConsole.Log(string.Format("can not find resourece config!name:{0}", res));
            if (callback != null) callback((int)RequestResultCode.Fail);
            return;
        }

        if(resources.ContainsKey(res))
        {
            if (callback != null) callback((int)RequestResultCode.Succees);
            return;
        }

        LoadResourceTask task = null;
        if(loadingTaskDict.TryGetValue(res,out task))
        {
            task.callback += callback;
        }
        else
        {
            task = new LoadResourceTask(res);
            task.callback += callback;
            task.releaseCallBack = TaskEnd;
            loadingTaskDict.Add(res, task);
            task.Start();
            
        }
    }

    /// <summary>
    /// 异步请求某个批次资源
    /// </summary>
    /// <param name="res"></param>
    /// <param name="callback"></param>
    public void RequeseBatchReourceAsync(List<string> resList,Action<int> callback,Action<float> progress = null)
    {
        if(resList.Count == 0)
        {
            if (callback != null) callback((int)RequestResultCode.Fail);
            return;
        }

        List<string> needLoadList = new List<string>();
        for(int i = 0;i< resList.Count;i++)
        {
            if(resourceConfig.ContainsKey(resList[i]) && !resources.ContainsKey(resList[i]))
            {
                needLoadList.Add(resList[i]);
            }
        }

        if(needLoadList.Count == 0 )
        {
            if (callback != null) callback((int)RequestResultCode.Succees);
            return;
        }

        BatchLoadResourceTask task = new BatchLoadResourceTask(needLoadList);
        task.OnSubTaskEnd = BatchSubTaskEnd;
        task.callback = callback;
        task.progressHandler += progress;
        task.Start();
    }


    public UnityEngine.Object GetResource(string name)
    {
        UnityEngine.Object res = null;
        ManagedResource mRes = null;
        if(resources.TryGetValue(name, out mRes))
        {
            res = mRes.resourceObject;
            mRes.refCount++;
        }
        return res;
    }

    public bool HasResource(string name)
    {
        if (string.IsNullOrEmpty(name)) return false;
        return resources.ContainsKey(name);
    }

    public void ReleaseResourece(UnityEngine.Object res)
    {
        if (res == null) return;
        ManagedResource mRes = null;
        var enu = resources.GetEnumerator();
        while(enu.MoveNext())
        {
            if(enu.Current.Value.resourceObject == res)
            {
                mRes = enu.Current.Value;
                break;
            }
        }
        if (mRes == null)
        {
            DebugConsole.Log(string.Format("release resource fail cannot find managed resource! name:{0}",res.name));
            return;
        }

        mRes.refCount--;
        //todo manager ab
    }

    public string GetABName(string resName)
    {
        if(resourceConfig.ContainsKey(resName))
        {
            return resourceConfig[resName];
        }
        return string.Empty;
    }

    /// <summary>
    /// 初始化本地的资源配置
    /// </summary>
    public void InitLocalConfig()
    {
        string path = AppPlatform.GetRuntimePackagePath() + "res_config.txt";
        using (StreamReader sr = new StreamReader(path, Encoding.UTF8))
        {
            string line = string.Empty;
            string abName = string.Empty;
            string resName = string.Empty;
            while(true)
            {
                if (sr.EndOfStream) break;
                line = sr.ReadLine().Trim('\t',' ','\n',',','\0');
                if(string.IsNullOrEmpty(line) || line.EndsWith("{"))
                {
                    continue;
                }
                if(line.EndsWith("}"))
                {
                    abName = string.Empty;
                    continue;
                }

                if (line.EndsWith(":"))
                {
                    abName = line.Substring(0, line.Length - 1);
                }else
                {
                    resName = line;
                    if(resourceConfig.ContainsKey(resName))
                    {
                        DebugConsole.Log(string.Format("resource config reqpeat!!ab:{0} ab2{1}", resName, resourceConfig[resName]));
                    }else
                    {
                        resourceConfig.Add(resName, abName);
                    }
                }
            }
            sr.Close();
        }
    }

    
    private void TaskEnd(LoadResourceTask task)
    {
        if (!task.IsComplete) return;
        loadingTaskDict.Remove(task.reousrceName);
        if (task.IsSucceed)
        {
            var res = CreateManagedResource(task);
            resources.Add(res.name,res);
        }
        if(task.callback != null) task.callback(task.IsSucceed ? (int)RequestResultCode.Succees : (int)RequestResultCode.Fail);
        task.Reset();
        task = null;
    }

    private void BatchSubTaskEnd(LoadResourceTask task,bool isLast)
    {
        if (task.IsSucceed)
        {
            var res = CreateManagedResource(task);
            resources.Add(res.name, res);
        }
        if (isLast)
        {
            if(task.callback != null) task.callback((int)RequestResultCode.Succees);
        }
    }

    private ManagedResource CreateManagedResource(LoadResourceTask task)
    {
        ManagedResource res = new ManagedResource();
        res.name = task.reousrceName;
        res.abName = GetABName(res.name);
        res.resourceObject = task.Resource;
        return res;
    }

    public static ResourceManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = new ResourceManager();
            return _instance;
        }
    }


    private class LoadResourceTask
    {
        public string reousrceName;
        public Action<int> callback;
        public Action<LoadResourceTask> releaseCallBack;
        public event Action<float> progressHandler;
        protected bool isComplete;
        protected bool isSucceed;
        protected float progress;
        protected UnityEngine.Object resource;
        public LoadResourceTask(string resName)
        {
            reousrceName = resName;
        }

        public void Start()
        {
            string abName = ResourceManager.Instance.GetABName(reousrceName);
            string shortResName = System.IO.Path.GetFileName(reousrceName);
            Global.AssetLoadManager.LoadAsset(abName, shortResName, OnLoadComplete,OnProgress);
        }
        public void Reset()
        {
            callback = null;
            isComplete = false;
            isSucceed = false;
            progress = 0f;
            progressHandler = null;
            resource = null;
        }

        public bool IsComplete
        {
            get
            {
                return isComplete;
            }
        }

        public bool IsSucceed
        {
            get
            {
                return isSucceed;
            }
        }

        public float Progress
        {
            get
            {
                return progress;
            }
        }

        public UnityEngine.Object Resource
        {
            get
            {
                return resource;
            }
        }
        
        public void OnLoadComplete(UnityEngine.Object obj)
        {
            isComplete = true;
            isSucceed = obj != null;
            if(!isSucceed)
            {
                string abName = ResourceManager.Instance.GetABName(reousrceName);
                DebugConsole.Log(string.Format("load resource fail,name:{0} anName:{1}", reousrceName, abName));
            }
            resource = obj;
            releaseCallBack(this);
        }

        void OnProgress(float p)
        {
            progress = p;
            if (progressHandler != null)
            {
                progressHandler(p);
            }
        }
    }

    private class BatchLoadResourceTask
    {
        private List<string> loadList;
        public Action<int> callback;
        public Action<LoadResourceTask,bool> OnSubTaskEnd;
        public event Action<float> progressHandler;
        private bool isComplete;
        private List<LoadResourceTask> tasks;
        private int subTaskCount;
        private LoadResourceTask crtTask;
        private float progress;

        public BatchLoadResourceTask(List<string> loadList)
        {
            this.loadList = loadList;
        }

        public void Rest()
        {
            isComplete = false;
            callback = null;
        }

        public void Start()
        {
            tasks = new List<LoadResourceTask>();
            LoadResourceTask task = null;
            subTaskCount = loadList.Count;
            for (int i = 0; i  < loadList.Count;i++)
            {
                task = new LoadResourceTask(loadList[i]);
                task.releaseCallBack = OnSubTaskEndHandler;
                task.Start();
                task.progressHandler += OnSubTaskProgressHandler;
                tasks.Add(task);
            }
        }

        private void OnSubTaskEndHandler(LoadResourceTask task)
        {
            if (task.IsComplete)
            {
                tasks.Remove(task);
                task.callback = callback;
                OnSubTaskEnd(task,tasks.Count == 0);
            }
            task.Reset();
        }

        private void OnSubTaskProgressHandler(float p)
        {
            progress = subTaskCount - tasks.Count; 
            for (int i = 0; i < tasks.Count; i++)
            {
                progress += tasks[i].Progress;
            }
            progress /= subTaskCount;
            if(progressHandler != null)
            {
                progressHandler(progress);
            }
        }

        public bool IsComplete
        {
            get
            {
                return isComplete;
            }
        }


        public float Progress
        {
            get
            {
               
                return progress;
            }
        }
    }

} 



public class BatchResourceRequestWrap
{
    private List<string> resourceNameList;
    public BatchResourceRequestWrap()
    {
        resourceNameList = new List<string>();
    }

    public void Push(string resName)
    {
        resourceNameList.Add(resName);
    }

    public bool RequestResource()
    {
        return ResourceManager.Instance.RequestBatchResource(resourceNameList);
    }

    public void RequestResourceAsync(Action<int> callback,Action<float> progressHandler = null)
    {
        ResourceManager.Instance.RequeseBatchReourceAsync(resourceNameList, callback,progressHandler);
    }
}




