﻿using UnityEngine;

/// <summary>
/// 管理器名
/// </summary>
public class ManagerName
{
    public const string Sound = "SoundManager";
    public const string AssetLoad = "AssetLoadManager";
    public const string Task = "TaskManager";
    public const string Gesture = "GestureManager";
    public const string Game = "GameManager";
}

/// <summary>
/// UI模板名
/// </summary>
public class TemplateName
{
    public const string DialogBox = "DialogBoxTemplate";
}

