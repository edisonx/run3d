﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Text;

public class PackageUtil
{
    private static Dictionary<string, AssetBundleBuild> abBuildList = new Dictionary<string, AssetBundleBuild>();
    private static StreamWriter resConfigSW;
    public static void GeneratorChecklist()
    {
        string newFilePath = PackagePlatform.GetPackagePath() + Global.PackageManifestFileName;
        if (File.Exists(newFilePath)) File.Delete(newFilePath);
        
        List<string> files = new List<string>();
        Util.RecursiveDir(PackagePlatform.GetPackagePath(), ref files);

        FileStream fs = new FileStream(newFilePath, FileMode.CreateNew);
        StreamWriter sw = new StreamWriter(fs);
        for (int i = 0; i < files.Count; i++)
        {
            string file = files[i];
            if (file.EndsWith(".meta") || file.Contains(".DS_Store") || file.EndsWith(".manifest")) continue;

            string md5 = Util.MD5File(file);
            string value = file.Replace(PackagePlatform.GetPackagePath(), string.Empty);
            sw.WriteLine(value + "|" + md5);
        }
        sw.Close(); fs.Close();
        AssetDatabase.Refresh();
    }

    public static void GeneratorVersion(string mainVersion, string minorVersion)
    {
        string newFilePath = PackagePlatform.GetPackagePath() + Global.PackageVersionFileName;
        if (File.Exists(newFilePath)) File.Delete(newFilePath);
        FileStream fs = new FileStream(newFilePath, FileMode.CreateNew);
        StreamWriter sw = new StreamWriter(fs);

        string version = mainVersion + '.' + minorVersion;
        sw.WriteLine(version);
        sw.Close(); fs.Close();
        AssetDatabase.Refresh();
    }


    public static void StartResConfig()
    {
        string newFilePath = PackagePlatform.GetPackagePath() + "res_config.txt";
        if (File.Exists(newFilePath)) File.Delete(newFilePath);

        resConfigSW = new StreamWriter(newFilePath,false,Encoding.UTF8);
    }

    /// <summary>
    /// 记录每个AB打包信息
    /// </summary>
    /// <param name="abbuild"></param>
    public static void PushResABBuild(AssetBundleBuild abbuild)
    {
        //分析AB是否需要写入到配置文件
        if (abbuild.assetBundleName == "lua.unity3d") return;
        string rawResourceDir = "Assets/RawResources/";
        string assetName = string.Empty;
        List<string> tempList = new List<string>();
        for (int i = 0; i < abbuild.assetNames.Length; i++)
        {
            assetName = abbuild.assetNames[i];
            if(assetName.Contains(rawResourceDir))
            {
                assetName = assetName.Replace(rawResourceDir, "");
            }else
            {
                assetName = assetName.Replace("Assets/", "");
            }
            string extension = System.IO.Path.GetExtension(assetName);
            if(!string.IsNullOrEmpty(extension))
            {
                assetName = assetName.Replace(extension, "");
            }
            tempList.Add(assetName);
        }

        if (tempList.Count == 0) return;
        resConfigSW.WriteLine(abbuild.assetBundleName + ":\n{");
        string suffix;
        for(int j = 0; j < tempList.Count;j++)
        {
            suffix = j == tempList.Count - 1 ? "" : ",\n";
            resConfigSW.WriteLine("\t" + tempList[j] + suffix);
        }
        resConfigSW.WriteLine("}");
    }

    public static void EndResConfig()
    {
        //add server resource config
        string path = Application.dataPath + "/RawResources/server_res_config.txt";
        using (StreamReader sr = new StreamReader(path, Encoding.UTF8))
        {
            resConfigSW.Write(sr.ReadToEnd());
        }
        resConfigSW.Flush();
        resConfigSW.Close();
        resConfigSW.Dispose();
        resConfigSW = null;
        AssetDatabase.Refresh();
    }

}
