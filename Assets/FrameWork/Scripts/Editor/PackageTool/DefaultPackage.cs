﻿using UnityEngine;
using UnityEditor;
using System.IO;


public class DefaultPackage : MonoBehaviour
{
    public static void BuildDefaultWindows()
    {
        BuildDefaultSpecify(PackagePlatform.PlatformType.Windows);
    }

    public static void BuildDefaultIOS()
    {
        BuildDefaultSpecify(PackagePlatform.PlatformType.IOS);
    }

    public static void BuildDefaultAndroid()
    {
        BuildDefaultSpecify(PackagePlatform.PlatformType.Android);
    }

    public static void BuildDefaultSpecify(PackagePlatform.PlatformType rBuildPlatform)
    {
        //打包DataTable
        if (Directory.Exists("Assets/RawResources/DataTable"))
        {
            Debug.Log("打包 DataTable");

            string outPath = PackagePlatform.GetPackagePath() + "DataTable";
            if (Directory.Exists(outPath))
            {
                Directory.Delete(outPath, true);
            }
            Directory.CreateDirectory(outPath);

            string inPath = "Assets/RawResources/DataTable";
            string[] files = Directory.GetFiles(inPath);

            float doneCount = 0f;
            for (int i = 0; i < files.Length; i++)
            {
                string file = files[i];
                if (file.Contains(".meta")) continue;
                Debug.Log(file);

                string outfile = file.Replace("Assets/RawResources", PackagePlatform.GetPackagePath());

                if (File.Exists(outfile)) File.Delete(outfile);

                File.Copy(file, outfile, true);


                doneCount++;
                float p = (doneCount / (float)files.Length);
                EditorUtility.DisplayProgressBar("Package", "Package Default Resources", p);
            }
            EditorUtility.ClearProgressBar();
        }

        PackageLuaCode(rBuildPlatform);
        CopyVuforiaData();
        ////打包Code
        //if (Directory.Exists("Assets/RawResources/Code"))
        //{
        //    Debug.Log("打包 Code");

        //    string outPath = PackagePlatform.GetPackagePath() + "Code";
        //    if (Directory.Exists(outPath))
        //    {
        //        Directory.Delete(outPath, true);
        //    }
        //    Directory.CreateDirectory(outPath);

        //    string inPath = "Assets/RawResources/Code";
        //    string[] files = Directory.GetFiles(inPath);

        //    float doneCount = 0f;
        //    for (int i = 0; i < files.Length; i++)
        //    {
        //        string file = files[i];
        //        if (file.Contains(".meta")) continue;
        //        Debug.Log(file);

        //        string outfile = file.Replace("Assets/RawResources", PackagePlatform.GetPackagePath());

        //        if (File.Exists(outfile)) File.Delete(outfile);

        //        File.Copy(file, outfile, true);


        //        doneCount++;
        //        float p = (doneCount / (float)files.Length);
        //        EditorUtility.DisplayProgressBar("Package", "Package Default Resources", p);
        //    }
        //    EditorUtility.ClearProgressBar();
        //}
    }

    static void PackageLuaCode(PackagePlatform.PlatformType targetPlatform)
    {
        string toluaPath = "Assets/ToLua/ToLua/Lua";
        string luaScriptPath = "Assets/ToLua/LuaScripts";
        if (Directory.Exists(toluaPath) && Directory.Exists(luaScriptPath))
        {
            string outPath = PackagePlatform.GetPackagePath() + "Code";
            if (Directory.Exists(outPath))
            {
                Directory.Delete(outPath, true);
            }
            Directory.CreateDirectory(outPath);

            string[] files = Directory.GetFiles(toluaPath,"*.lua", SearchOption.AllDirectories);
            ArrayUtility.AddRange<string>(ref files, Directory.GetFiles(luaScriptPath, "*.lua", SearchOption.AllDirectories));

            string tempPath = Application.dataPath + "/PackageTemp";
            if (Directory.Exists(tempPath))
            {
                Directory.Delete(tempPath, true);
            }
            Directory.CreateDirectory(tempPath);

            string tempName = string.Empty;
            string destPath = string.Empty;
            string[] newFiles = new string[files.Length];
            for (int i = 0; i < files.Length; i++)
            {
                string file = files[i];
                file = file.Replace("\\", "/");
                destPath = file.Replace("Assets/ToLua/", "") + ".bytes";
                if (destPath.Contains("ToLua/Lua/"))
                {
                    destPath = destPath.Replace("ToLua/Lua/", "");
                }

                if (destPath.Contains("LuaScripts/"))
                {
                    destPath = destPath.Replace("LuaScripts/", "");
                }

                destPath = tempPath + "/" + destPath.Replace("/","_");
                //destPath = Path.ChangeExtension(destPath, ".bytes");
                if (!Directory.Exists(Path.GetDirectoryName(destPath))) Directory.CreateDirectory(Path.GetDirectoryName(destPath));
                File.Copy(file, destPath);
                newFiles[i] = destPath.Replace(Application.dataPath,"Assets");
            }

            AssetBundleBuild build = new AssetBundleBuild();
            build.assetBundleName = "lua.unity3d";
            build.assetNames = newFiles;

            var builds = new AssetBundleBuild[] { build };
            AssetDatabase.Refresh();
            BuildPipeline.BuildAssetBundles(outPath, builds, BuildAssetBundleOptions.None, PackagePlatform.GetBuildTarget());
            Directory.Delete(tempPath, true);
        }
        else
        {
            Debug.Log("打包lua代码失败，代码目录异常");
        }
    }

    static void CopyVuforiaData()
    {
        string outPath = PackagePlatform.GetPackagePath() + "/YouLiaoAR/";
        if (!Directory.Exists(outPath))
            Directory.CreateDirectory(outPath);
        string inPath = Application.dataPath + "/YouLiaoAR";
        string[] files = Directory.GetFiles(inPath);
        string file = null;
        string fileName = null;
        for(int i = 0;i<files.Length;i++)
        {
            file = files[i];
            if (file.EndsWith(".meta")) continue;
            fileName = Path.GetFileName(file);
            File.Copy(file, outPath + fileName,true);
        }
    }
}
