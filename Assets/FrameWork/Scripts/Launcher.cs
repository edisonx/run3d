﻿using UnityEngine;
using System.Collections;
using System;

public class Launcher : MonoBehaviour {

    void Awake() {

    }

    void Start() {
        GameController.Instance.Initialize(Startup);
    }

    void Startup() {

        //LShapClient.Instance.Initialize();

        DebugConsole.Log("[Framework initialize complete]");
        DebugConsole.Log("[Assetbundle prepare complete]");
        DebugConsole.Log("[LShape initialize complete]");
        DebugConsole.Log("[StreamingAssetsPath]：" + Application.streamingAssetsPath);
        DebugConsole.Log("[RuntimeAssetsPath] :" + AppPlatform.RuntimeAssetsPath);
        DebugConsole.Log("[AssetBundleDictionaryUrl]:" + Global.AssetLoadManager.DownloadingURL);

        //程序初始化完毕， 进入游戏
        try {
            var maintick = Global.MainTick;
            StartCoroutine(StartupLua());
        } catch (Exception e) {
            DebugConsole.Log(e.Message + e.StackTrace);
        }
    }

    IEnumerator StartupLua() {
        yield return new WaitForEndOfFrame();
//#if !UNITY_EDITOR
//        LuaInterface.LuaFileUtils.Instance.beZip = true;
//        var codeBytes = FileUtil.ReadFileWithByte(AppPlatform.GetRuntimePackagePath() + "Code/" + "lua.unity3d");
//        AssetBundle ab = AssetBundle.LoadFromMemory(codeBytes);
//        LuaInterface.LuaFileUtils.Instance.AddSearchBundle("lua", ab);
//#endif
        //Global.MainTick.gameObject.AddComponent<NativeListener>();
        //var luaClient = Global.MainTick.gameObject.AddComponent<LuaClient>();
        Destroy(gameObject);
    }
}
