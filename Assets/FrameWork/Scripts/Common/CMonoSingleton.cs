﻿using UnityEngine;
using System.Collections;

/// <summary>
/// C mono singleton.
/// </summary>
public abstract class CMonoSingleton<T> : MonoBehaviour where T : CMonoSingleton<T> {

	protected static T instance = null;

	public static T GetInstance()
	{
		if (instance == null) {
			instance = FindObjectOfType<T> ();
			if (FindObjectsOfType<T> ().Length > 0) {
				Debug.LogError (typeof(T).Name + " more than 1!");
				return instance;
			}
			if (instance == null) {
				string instanceName = typeof(T).Name;
				Debug.Log ("Instance Name : " + instanceName);
				GameObject instanceGo = GameObject.Find (instanceName);
				if (instanceGo == null) {
					instanceGo = new GameObject (instanceName);
				}
				instance = instanceGo.AddComponent<T> ();
				DontDestroyOnLoad (instanceGo);
				Debug.Log ("Add new monosingleton " + instance.name + " in scene!");
			} else {
				Debug.Log ("Already exist : " + instance.name);
			}
		}
		return instance;
	}

	protected virtual void OnDestroy()
	{
		instance = null;
	}

}
