﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 范型 反射 抽象类
/// </summary>
public abstract class CSingleton<T> where T : CSingleton<T> {

	protected static T instance = null;

	protected CSingleton()
	{
		
	}

	/// <summary>
	/// Gets the instance.
	/// </summary>
	/// <returns>The instance.</returns>
	public static T GetInstance()
	{
		if (instance == null) {
			ConstructorInfo[] ctors = typeof(T).GetConstructors (BindingFlags.Instance | BindingFlags.NonPublic);
			ConstructorInfo ctor = Array.Find (ctors, c => c.GetParameters ().Length == 0);
			if (ctor == null)
				throw new Exception ("Non-public ctor() not found!");
			instance = ctor.Invoke (null) as T;
		}
		return instance;
	}

}
